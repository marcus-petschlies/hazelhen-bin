#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log
echo "# [$MyName] (`date`)" | tee $log

Conf=()

path_to_data=.


ref_size=2038432496

ref_idx_min=0
ref_idx_max=11

source $MyName.in

if [ "X$ref_size" == "X" ] || [ "X$ref_idx_min" == "X" ] || [ "X$ref_idx_max" == "X" ]; then
  echo "[$MyName] Error, need ref_size, ref_idx_min and ref_idx_max"
  exit 1
fi

cat << EOF | tee -a $log
# [$MyName] path_to_data = $path_to_data
# [$MyName]
# [$MyName] ref_size     = $ref_size
# [$MyName]
# [$MyName] ref_idx_min  = $ref_idx_min
# [$MyName] ref_idx_max  = $ref_idx_max
# [$MyName]
# [$MyName] Conf         = ${Conf[*]}

EOF
sleep 1s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

printf "%8s %20s %20s %8s %8s\n" "conf" "src" "prp" "src all" "prp all" | tee -a $log
printf "# =======================================================================================================================================\n\n" | tee -a $log

for g in ${Conf[*]}; do
  gfmt=`printf "%.4d" $g`

  src_ok=""

  for((s=$ref_idx_min; s<=$ref_idx_max; s++)); do

    f=$path_to_data/$g/source.${gfmt}.`printf "%.5d" $s`

    if ! [ -e $f ]; then
      src_ok="${src_ok}0"
    else
      fsize=$(stat -c %s $f)

      if [ $fsize -lt $ref_size ]; then
        src_ok="${src_ok}0"
      else
        src_ok="${src_ok}1"
      fi
    fi

  done  # end of loop on stochastic samples

  prp_ok=""

  for((s=$ref_idx_min; s<=$ref_idx_max; s++)); do

    f=$path_to_data/$g/prop.${gfmt}.`printf "%.5d" $s`

    if ! [ -e $f ]; then
      prp_ok="${prp_ok}0"
    else
      fsize=$(stat -c %s $f)

      if [ $fsize -lt $ref_size ]; then
        prp_ok="${prp_ok}0"
      else
        prp_ok="${prp_ok}1"
      fi
    fi
  done  # of loop on stochastic samples

  src_all_ok=
  src_all_ok=$( echo ${src_ok} | awk '$1 ~ "0" {print 0}; ! ( $1 ~ "0" ) {print 1}' )

  prp_all_ok=
  prp_all_ok=$( echo ${prp_ok} | awk '$1 ~ "0" {print 0}; ! ( $1 ~ "0" ) {print 1}' )

  printf "%8d %20s %20s %8d %8d\n" $g $src_ok $prp_ok $src_all_ok $prp_all_ok
  printf "# ---------------------------------------------------------------------------------------------------------------------------------------\n"
done 2>&1 | tee -a $log

echo "# [$MyName] (`date`) all done" | tee -a $log

exit 0
