#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log
echo "# [$MyName] `date`" > $log

TPAUSE=1

Conf=( )

prefix_list=( "N-N" "D-D" "pixN-D" "pixN-pixN" )

pref_str_list=( "PX0_PY0_PZ0" "PX0_PY0_PZ1" "PX1_PY1_PZ0" "PX1_PY1_PZ1" )

ref_size_list=( \
  "111763,668729,1337099,891531,"\
  "445813,2672309,5344115,3562923,"\
  "6015024,24056030,32075513,14257549,"\
  "324739066,865970050,769782658,228106882,"\
)

suffix=aff

if [ -e ${MyName}.in ]; then
  source ${MyName}.in
fi

cat << EOF | tee -a $log

# [$MyName] prefix_list = ${prefix_list[*]}
# [$MyName] 
# [$MyName] pref_str_list        = ${pref_str_list[*]}
# [$MyName] 
# [$MyName] ref_size_list        = ${ref_size_list[*]}
# [$MyName] 
# [$MyName] Conf                 = ${Conf[*]}

EOF
sleep ${TPAUSE}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

for i in 0; do
  printf "# %4s %10s %16s %3s %4s %3s\n" "conf" "prefix" "pref_str" "aff" "size" "all"
  printf "# ================================================================\n"
done 2>&1 | tee -a $log


prefix_num=$(echo ${prefix_list[*]} | wc -w)

pref_num=$(echo ${pref_str_list[*]} | wc -w)

for g in ${Conf[*]}; do
  gfmt=`printf "%.4d" $g`

  conf_ok=1

  for((i=0; i<$prefix_num; i++)); do

    prefix=${prefix_list[$i]}

    ref_size=( $(echo ${ref_size_list[$i]} | tr ',' ' ' ) )

    for((k=0; k<$pref_num;k++)); do
    
      pref_str=${pref_str_list[$k]}

      filename=${g}/${prefix}.${pref_str}.${gfmt}.${suffix}

      aff_ok=1
      size_ok=1

      if ! [ -e $filename ]; then
        aff_ok=0
        size_ok=0
      else
        lhpc-aff ls $filename 1>/dev/null 2>/dev/null
        es=$?
        if [ $es -ne 0 ]; then
          aff_ok=0
        fi

        s=$(stat -c %s ${filename} 2>/dev/null )
        if [ $s -lt ${ref_size[$k]} ]; then
          size_ok=0
        fi
      fi

      all_ok=$(( $aff_ok & $size_ok ))

      printf "%6d %10s %16s %3d %4d %3d\n" $g $prefix $pref_str $aff_ok $size_ok $all_ok

      conf_ok=$(($conf_ok & $all_ok))
    done
  done

  printf "# ----------------------------------------------------------------\n"
  printf "conf %6d %3s %2d\n" $g "ok" $conf_ok
  printf "# ----------------------------------------------------------------\n"
done 2>&1 | tee -a $log

echo "# [$MyName] `date`" >> $log

exit 0
