#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log
echo "# [$MyName] `date`" > $log

TPAUSE=1

Conf=(  )

nsrc_base_ref_number=4
nsrc_cohe_ref_number=2
prefix=piN_piN_diagrams

ptot_str_list=( "PX0PY0PZ0" "PX0PY0PZ1" "PX1PY1PZ0" "PX1PY1PZ1" )

b_ref_size=( 301150333 803066509 713857933 211529101 )

d_ref_size=(2789309 16728345 33455199 22303975 )

### m_ref_size=( 1917 10193 20135 13519 )
m_ref_size=( 1895 10091 19937 13385 )

n_ref_size=( 413681 2479237 4957915 3305475 )

s_ref_size=( 301150333 803066509 713857933 211529101 )

t_ref_size=( 50192713 200766915 267695934 118982450 ) 

w_ref_size=( 602298209 1606130561 1427713409 423055745 )

z_ref_size=( 602298209 1606130561 1427713409 423055745 ) 



b_rel_number=1

d_rel_number=1

m_rel_number=1

n_rel_number=1

s_rel_number=1

t_rel_number=1

w_rel_number=1

z_rel_number=1


source ${MyName}.in

nsrc_ref_number=$(( $nsrc_base_ref_number * $nsrc_cohe_ref_number ))

b_ref_number=$(( $b_rel_number * $nsrc_ref_number ))
d_ref_number=$(( $d_rel_number * $nsrc_ref_number ))
m_ref_number=$(( $m_rel_number * $nsrc_ref_number ))
n_ref_number=$(( $n_rel_number * $nsrc_ref_number ))
s_ref_number=$(( $s_rel_number * $nsrc_ref_number ))
t_ref_number=$(( $t_rel_number * $nsrc_ref_number ))
w_ref_number=$(( $w_rel_number * $nsrc_ref_number ))
z_ref_number=$(( $z_rel_number * $nsrc_ref_number ))


cat << EOF | tee -a $log

# [$MyName] prefix = $prefix
# [$MyName] 
# [$MyName] nsrc_base_ref_number = $nsrc_base_ref_number
# [$MyName] nsrc_cohe_ref_number = $nsrc_cohe_ref_number
# [$MyName] nsrc_ref_number      = $nsrc_ref_number
# [$MyName] 
# [$MyName] ptot_str_list        = ${ptot_str_list[*]}
# [$MyName] 
# [$MyName] b_ref_size           = ${b_ref_size[*]}
# [$MyName] b_ref_number         = ${b_ref_number}
# [$MyName] 
# [$MyName] w_ref_size           = ${w_ref_size[*]}
# [$MyName] w_ref_number         = ${w_ref_number}
# [$MyName] 
# [$MyName] z_ref_size           = ${z_ref_size[*]}
# [$MyName] z_ref_number         = ${z_ref_number}
# [$MyName] 
# [$MyName] s_ref_size           = ${s_ref_size[*]}
# [$MyName] s_ref_number         = ${s_ref_number}
# [$MyName] 
# [$MyName] t_ref_size           = ${t_ref_size[*]}
# [$MyName] t_ref_number         = ${t_ref_number}
# [$MyName] 
# [$MyName] n_ref_size           = ${n_ref_size[*]}
# [$MyName] n_ref_number         = ${n_ref_number}
# [$MyName] 
# [$MyName] d_ref_size           = ${d_ref_size[*]}
# [$MyName] d_ref_number         = ${d_ref_number}
# [$MyName] 
# [$MyName] m_ref_size           = ${m_ref_size[*]}
# [$MyName] m_ref_number         = ${m_ref_number}
# [$MyName] 
# [$MyName] Conf                 = ${Conf[*]}

EOF
sleep ${TPAUSE}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi



function check_file_list {
	# args g (1) b (2) n (3) ref_size (4,5,...)

  nptot=$(echo ${ptot_str_list[*]} | wc -w)

  file_number_ok=""
  file_size_ok=""
  ref_number=$3

  for((i=0; i<$nptot; i++));
  do
    ii=$(( $i + 4 ))
    ref_size=${!ii}
    # name=$(printf "%s.%s.%s.%.4d.*.aff" ${prefix} $2 ${ptot_str_list[$i]} $g)
    # echo "name = $name"
    file_list=($( ls $(printf "%d/%s.%s.%s.%.4d.*.aff" $1 ${prefix} $2 ${ptot_str_list[$i]} $1) 2>/dev/null ))
    # echo "file_list = ${file_list[*]}"
    nf=$(echo ${file_list[*]} | wc -w)
    if [ $nf -eq ${ref_number} ]; then
      file_number_ok="${file_number_ok}1"
    else
      file_number_ok="${file_number_ok}0"
    fi
    t=
    if [ $nf -eq ${ref_number} ]; then
      t=1
    else
      t=0
    fi
    for f in ${file_list[*]}; do
      s=$(stat -c %s ${f} 2>/dev/null )
      if [ $s -lt ${ref_size} ]; then t=0; fi
    done
    file_size_ok="${file_size_ok}$t"
  done
  echo "$1 $2 ${file_number_ok} ${file_size_ok}"
}

for i in 0; do
  printf "#%5s  %3s  %8s  %8s\n" "conf" "top" "num ok" "size ok"
  printf "# ================================================================\n"
done 2>&1 | tee -a $log


for g in ${Conf[*]}; do
  gfmt=`printf "%.4d" $g`

  check_b=($( check_file_list $g "b" ${b_ref_number} ${b_ref_size[*]} ))
  check_w=($( check_file_list $g "w" ${w_ref_number} ${w_ref_size[*]} ))
  check_z=($( check_file_list $g "z" ${z_ref_number} ${z_ref_size[*]} ))
  check_s=($( check_file_list $g "s" ${s_ref_number} ${s_ref_size[*]} ))
  check_t=($( check_file_list $g "t" ${t_ref_number} ${t_ref_size[*]} ))
  check_n=($( check_file_list $g "n" ${n_ref_number} ${n_ref_size[*]} ))
  check_d=($( check_file_list $g "d" ${d_ref_number} ${d_ref_size[*]} ))
  check_m=($( check_file_list $g "m" ${m_ref_number} ${m_ref_size[*]} ))

  printf "%6s  %3s  %8s  %8s\n" ${check_b[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_w[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_z[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_s[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_t[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_n[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_d[*]}
  printf "%6s  %3s  %8s  %8s\n" ${check_m[*]}

  allok=$( [[ "X$(echo ${check_b[2]} ${check_b[3]} ${check_w[2]} ${check_w[3]} ${check_z[2]} ${check_z[3]} ${check_s[2]} ${check_s[3]} ${check_t[2]} ${check_t[3]} ${check_n[2]} ${check_n[3]} ${check_d[2]} ${check_d[3]} ${check_m[2]} ${check_m[3]} | grep 0 )" == "X" ]] && echo 1  )

  printf "%6d  %3s  %2d\n" $g "all"  $allok

  printf "# ----------------------------------------------------------------\n"
done 2>&1 | tee -a $log

echo "# [$MyName] `date`" >> $log

exit 0
