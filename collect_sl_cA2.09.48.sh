#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

ncoherent=2
T=96

Tc=$(( $T / $ncoherent ))
cat << EOF
# [$MyName] T          = $T
# [$MyName] ncoherent  = $ncoherent
# [$MyName] Tc         = $Tc
EOF

output_file=source_locations.cA2.09.48.all
echo "# [$MyName] (`date`)" > $output_file

input_file=source_locations.cA2.09.48.tbase8.ncoherent2.200_4_996.tab
echo "# [$MyName] input file = $input_file" >> $output_file
awk '/^#/ {print; next}' $input_file        >> $output_file
for d in $(seq 200 4 996); do
  awk '$1=='$d' && $2<'$Tc'' $input_file  | awk '(NR-1)%2==0'
done                                        >> $output_file

input_file=source_locations.cA2.09.48.tbase8.ncoherent2.1000_4_1696.tab
echo "# [$MyName] input file = $input_file" >> $output_file
awk '/^#/ {print; next}' $input_file        >> $output_file
for d in $(seq 1000 4 1696); do
  awk '$1=='$d' && $2<'$Tc'' $input_file | awk '(NR-1)%2==0'
done                                        >> $output_file

input_file=source_locations.cA2.09.48.tab
echo "# [$MyName] input file = $input_file" >> $output_file
awk '/^#/ {print; next}' $input_file        >> $output_file
for d in $(seq 1700 4 2704); do
  awk '$1=='$d'' $input_file
done                                        >> $output_file

echo "# [$MyName] (`date`)"                 >> $output_file

exit 0
