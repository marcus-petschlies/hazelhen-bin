#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
log=$MyName.log

sigma_g0d=( 1 -1 -1 -1  1 -1  1 -1 -1 -1  1  1  1 -1 -1 -1 )

########################################
# get irrep dimension from first
#   character of irrep name
########################################

itag=""
source_filename=""

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-t") itag=$2; shift 2;;
      "-s") source_filename=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi


echo "# [$MyName] (`date`)" | tee $log

if [ "X$itag" == "X" ] ; then
  echo "[$MyName] Error, need itag"  | tee -a $log
  exit 1
fi

if [ "X$source_filename" == "X" ] ; then
  source_filename="$MyName.in"
  echo "[$MyName] Warning, no source_filename given, trying default $source_filename" | tee -a $log
fi

if ! [ -e $source_filename ]; then
  echo "[$MyName] Warning, could not find source file $source_filename" | tee -a $log
  source_filename="NA"
else
  echo "# [$MyName] source from file $source_filename"  | tee -a $log
fi


########################################
########################################
##
## set default values
##
########################################
########################################

########################################
# time extent values
########################################

########################################
# number of sink timeslices
#  ( not including source time )
########################################
src_snk_time_separation=24
TT=96

########################################
########################################
##
## set default values
## for group projection
##
########################################
########################################

fbwd_list=( fwd bwd )
d_spin=4
reorder=0

if [ "X${source_filename}X" != "XNAX" ]; then
  source $source_filename
fi

########################################
# output filename
########################################
lst=$MyName.${itag}.lst

cat << EOF | tee -a $log
# [$MyName] itag                    = $itag
# [$MyName] source_filename         = $source_filename
# [$MyName] src_snk_time_separation = $src_snk_time_separation
# [$MyName]
# [$MyName] d_spin                  = $d_spin
# [$MyName] reorder                 = $reorder
# [$MyName] fbwd_list               = ${fbwd_list[*]}
# [$MyName] 
# [$MyName] list filename           = $lst
# [$MyName] 
EOF

########################################
########################################
##
## start the list file
##
########################################
########################################

########################################
# initialize new projector list file
########################################
echo "# [$MyName] (`date`)" > $lst

########################################
#
if [ "X$itag" == "Xm-m" ]
#
########################################
then
  tag="m-m"
  type="m-m"

  gi2_list=( 5 )
  gf2_list=( 5 )

  diagrams="m1"
  d_spin=1

  if [ "X${source_filename}X" != "XNAX" ]; then
    source $source_filename  
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $(( $TT - 1 ))
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0

  for fbwd in ${fbwd_list[*]}; do

    for gi2 in ${gi2_list[*]}; do
    for gf2 in ${gf2_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  gi2      = $gi2
  gf2      = $gf2
  fbwd     = $fbwd
EndTwopointFunction

EOF
        counter=$(( $counter + 1 ))
    done  # of gf2
    done  # of gi2
  done  # of fbwd

echo "# [$MyName] number of $tag diagram entries $counter"

fi  # end of m-m

########################################
########################################

########################################
#
if [ "X$itag" == "XN-N" ] || [ "X$itag" == "Xall" ]; then
#
########################################

  tag="N-N"
  type="b-b"

  gi1_list=(  14,4  11,5  8,4 )
  gf1_list=(  14,4  11,5  8,4 )

  diagrams="n1,n2"
  d_spin=4

  if [ "X${source_filename}X" != "XNAX" ]; then
    source $source_filename  
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0

  for fbwd in ${fbwd_list[*]}; do

    for gi1 in ${gi1_list[*]}; do
    for gf1 in ${gf1_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  gi1      = $gi1
  gf1      = $gf1
  fbwd     = $fbwd
EndTwopointFunction

EOF
          counter=$(( $counter + 1 ))
    done  # of gf1
    done  # of gi1
  done  # of fbwd

echo "# [$MyName] number of $tag diagram entries $counter"

fi  # end of N-N

########################################
########################################


########################################
#
if [ "X$itag" == "XD-D" ] || [ "X$itag" == "Xall" ];
#
########################################
then

  tag="D-D"
  type="b-b"

  gi1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="d1,d2,d3,d4,d5,d6"
  d_spin=4

  ########################################
  # source input file
  ########################################
  if [ "X${source_filename}X" != "XNAX" ]; then
    source $source_filename  
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0

  for fbwd in ${fbwd_list[*]}; do

    for gi1 in ${gi1_list[*]}; do
          
      gi1_12=($( echo $gi1 | tr ',' ' ' ))

      norm_str="  # norm     ="
      s0d=${sigma_g0d[${gi1_12[0]}]}
      if [ $s0d -ne 1 ]; then
        norm_str="  norm     = $s0d"
        for((i=1; i<$num_diag; i++));do norm_str="${norm_str},${sigma_g0d[${gi1_12[0]}]}"; done
      fi

      for gf1 in ${gf1_list[*]}; do

        gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF >> $lst

BeginTwopointFunctionInit
  gi1      = $gi1
  gf1      = $gf1
  fbwd     = $fbwd
$norm_str
EndTwopointFunction

EOF
          counter=$(( $counter + 1 ))

      done  # of gf1
    done  # of gi1
  done  # of fbwd

echo "# [$MyName] number of $tag diagram entries $counter"

fi  # end of D-D

########################################
########################################

########################################
#
if [ "X$itag" == "XpixN-D" ] || [ "X$itag" == "Xall" ]
#
########################################
then

  tag="pixN-D"
  type="mxb-b"

  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="t1,t2,t3,t4,t5,t6"
  d_spin=4

  if [ "X${source_filename}X" != "XNAX" ]; then
    source $source_filename  
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

  counter=0

  # loop on forward, backward
  for fbwd in ${fbwd_list[*]}; do

    # loop on gamma structures
    for gi1 in ${gi1_list[*]}; do
          
      gi1_12=($( echo $gi1 | tr ',' ' ' ))

      for gi2 in ${gi2_list[*]}; do

      for gf1 in ${gf1_list[*]}; do

        gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF >> $lst

BeginTwopointFunctionInit
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  fbwd     = $fbwd
EndTwopointFunction

EOF

        counter=$(( $counter + 1 ))
      done  # of gf1
      done  # of gi2
    done  # of gi1
  done  # of fbwd

  echo "# [$MyName] number of $tag diagram entries $counter"

fi  # end of pixN-D

########################################
########################################

########################################
#
if [ "X$itag" == "XpixN-pixN" ] || [ "X$itag" == "Xall" ]
#
########################################
then

  tag="pixN-pixN"
  type="mxb-mxb"

  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=( 14,4  11,5  8,4 )
  gf2_list=(  5 )

  diagram_set_list=( "b1,b2" "w1,w2,w3,w4" "z1,z2,z3,z4" "s1,s2" )
  d_spin=4

  if [ "X${source_filename}X" != "XNAX" ]; then
    source $source_filename  
  fi

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
EndTwopointFunction

EOF

  counter=0

  # loop on diagram sets
  for  diagrams in ${diagram_set_list[*]}; do
 
    num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
    echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" >> $log


    # loop on forward, backward
    for fbwd in ${fbwd_list[*]}; do

      # loop on gamma structures
      for gi1 in ${gi1_list[*]}; do
          
        gi1_12=($( echo $gi1 | tr ',' ' ' ))

        for gi2 in ${gi2_list[*]}; do

          for gf1 in ${gf1_list[*]}; do

            gf1_12=($( echo $gf1 | tr ',' ' ' ))

            for gf2 in ${gf2_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  gf2      = $gf2
  fbwd     = $fbwd
  diagrams = $diagrams
  n        = $num_diag
EndTwopointFunction

EOF
              counter=$(( $counter + 1 ))

            done  # of gf2
          done  # of gf1
        done  # of gi2
      done  # of gi1
    done  # of fbwd
  done  # of loop on diagram sets

  echo "# [$MyName] number of $tag diagram entries $counter"

fi  # end of pixN-pixN

echo "# [$MyName] (`date`)" >> $lst

echo "# [$MyName] (`date`)" | tee -a $log
exit 0
