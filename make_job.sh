#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

Conf=( )

work_path=$PWD

source_coords_filename=

first_stoch_production=yes

stoch_path=

mode=7

with_submit=no
n_coherent_source=2
LL=
TT=
ensemble_name=

walltime=24:00:00
nodes=
ppn=24
nproc=

run_prefix="piN2piN_factorized"
JOB_LOG=$work_path/JOB_LOG

seed_offset=1000000

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-L") LL=$2; shift 2;;
      "-T") TT=$2; shift 2;;
      "-c") checkedInput=$2; shift 2;;
      "-n") n_coherent_source=$2; shift 2;;
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      "-w") walltime=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-f") first_stoch_production=$2; shift 2;;
      "-Nn") nodes=$2; shift 2;;
      "-Np") nproc=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

source ${MyName}.in

         job_mask=$work_path/job.sh
   cvc_input_mask=$work_path/cvc.input
tmLQCD_input_mask=$work_path/invert.input

cat << EOF
###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit            = $with_submit
# [$MyName] ensemble name          = $ensemble_name
# [$MyName] work_path              = $work_path
# [$MyName]
# [$MyName] L                      = $LL
# [$MyName] T                      = $TT
# [$MyName] source coords file     = $source_coords_filename
# [$MyName]
# [$MyName] job mask               = $job_mask
# [$MyName] input mask             = $cvc_input_mask
# [$MyName] tmLQCD_input_mask      = $tmLQCD_input_mask
# [$MyName]
# [$MyName] Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName] seed_offset            = $seed_offset
# [$MyName]
# [$MyName] n_coherent source      = $n_coherent_source
# [$MyName] first_stoch_production = $first_stoch_production
# [$MyName] mode                   = $mode
# [$MyName] stoch_path             = $stoch_path
# [$MyName]
# [$MyName] walltime               = $walltime
# [$MyName] nodes                  = $nodes
# [$MyName] ppn                    = $ppn
# [$MyName] nproc                  = $nproc

EOF

sleep 4s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Error, need source coords filename"
  exit 1
fi

if [ "X$nodes" == "X" ] || [ "X$nproc" == "X" ] || [ "X$ppn" == "X" ] ; then
  echo "# [$MyName] Error, need number of nodes, nproc, ppn"
  exit 1
fi

if [ "X$mode" == "X" ]; then
  echo "# [$MyName] Error, need mode"
  exit 1
fi

#  loop on gauge configurations
for g in ${Conf[*]}; do

  date_tag=`date +%Y_%m_%d_%H_%M_%S`

  gfmt=$(printf "%.4d" $g)

  WDIR=$work_path/$g
  mkdir -p $WDIR && cd $WDIR || exit 1

  source_coords_list=($(awk '$1=='$g' && $2<'$TT'/'$n_coherent_source' {printf("%d,%d,%d,%d ", $2,$3,$4,$5)}' $source_coords_filename))
  source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
###  echo "# [$MyName] source coords number = $source_coords_number"

cat << EOF
[$MyName]   start configuration  = $g
[$MyName]   date_tag             = $date_tag
[$MyName]   source_coords_list   = ${source_coords_list[*]}
[$MyName]   source_coords_number = $source_coords_number
[$MyName] 

EOF

  jobid=
  for (( isrc=0; isrc<$source_coords_number; isrc++));
  do

             job=${run_prefix}.${isrc}.${date_tag}.sh
       cvc_input=${run_prefix}.${isrc}.${date_tag}.input
    tmLQCD_input=invert.input
             out=${run_prefix}.${isrc}.${date_tag}.out
             err=${run_prefix}.${isrc}.${date_tag}.err
         job_out=job-${run_prefix}.${isrc}.${date_tag}.out
         job_err=job-${run_prefix}.${isrc}.${date_tag}.err
    stoch_io="-r -R"
    if [ $isrc -eq 0 ] && [ "X$first_stoch_production" == "Xyes" ] ; then
      stoch_io="-w -W"
    fi

cat << EOF
[$MyName]     job          = $job
[$MyName]     cvc_input    = $cvc_input
[$MyName]     tmLQCD_input = $tmLQCD_input
[$MyName]     out          = $out
[$MyName]     err          = $err
[$MyName]     stoch_io     = $stoch_io
[$MyName]

EOF

    cat $job_mask | awk '
      /^#PBS -N /         { print "#PBS -N '"$ensemble_name"'_prod_'$gfmt'_'$isrc'"; next}
      /^#PBS -l walltime/ { print "#PBS -l walltime='"$walltime"'"; next}
      /^#PBS -l nodes/    { print "#PBS -l nodes='$nodes':ppn='$ppn'"; next}
      /^#PBS -o /         { print "#PBS -o '$WDIR/$job_out'"; next}
      /^#PBS -e /         { print "#PBS -e '$WDIR/$job_err'"; next}
      /^workdir=/         { print "workdir='$WDIR'"; next}
      /^input=/           { print "input='$cvc_input'"; next}
      /^out=/             { print "out='$out'"; next}
      /^err=/             { print "err='$err'"; next}
      /^stoch_io=/        { print "stoch_io=\"'"$stoch_io"'\""; next}
      /^mode_of_op=/      { print "mode_of_op='"$mode"'"; next}
      /^nproc=/           { print "nproc='$nproc'"; next}
      /^ppn=/             { print "ppn='$ppn'"; next}
      {print}' > $job

    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '$g'"; next}
      /^seed[\ =]/            { print "seed = "('$seed_offset'+'$g'); next}
      /^filename_prefix[\ =]/  { print "filename_prefix = '"$stoch_path"'/'$g'/source"; next}
      /^filename_prefix2[\ =]/ { print "filename_prefix2 = '"$stoch_path"'/'$g'/prop"; next}
      {print}' > $cvc_input

    # append source coords list
    printf "source_coords = %s\n" ${source_coords_list[$isrc]} >> $cvc_input

    if [ $isrc -eq 0 ]; then
      cat $tmLQCD_input_mask | awk '
        /^InitialStoreCounter[\ =]/ {print "InitialStoreCounter = '$g'"; next}
        {print}' > $tmLQCD_input
    fi

    if [ "X$with_submit" == "Xyes" ]; then

      echo "# [$MyName] (`date`) $g $isrc" 2>&1 | tee -a $JOB_LOG
      if [ $isrc -eq 0 ]; then
        X=$(qsub $job)
        if [ "X$X" == "X" ]; then
          echo "[$MyName] Error submitting job $job"
          exit 3
        fi 2>&1 | tee -a $JOB_LOG
        echo $X >> JOB_LOG
        jobid=$(echo $X | awk -F. '{print $1}')
      else
        if [ "X$first_stoch_production" == "Xno" ]; then
          qsub $job 2>&1 | tee -a $JOB_LOG
        else
          if [ "X$jobid" == "X" ]; then
            echo "[$MyName] Error, no job id returned"
            exit 3
          else
            echo "# [$MyName] depend on jobid = $jobid"
          fi
          qsub -W depend=afterok:$jobid $job 2>&1 | tee -a $JOB_LOG
        fi
      fi
      sleep 3s

    fi  # end of if with submit
  done  # end of loop on base source locations

cat << EOF
[$MyName]   end configuration   = $g
EOF

  sleep 1s

done
echo "# [$MyName] (`date`)"
exit 0
