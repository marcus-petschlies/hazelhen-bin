#!/bin/bash
#PBS -N 
#PBS -l nodes=:ppn=
#PBS -l walltime=
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe
#PBS -o 
#PBS -e 
  
# Change to the direcotry that the job was submitted from

execdir=/zhome/academic/HLRS/hsk/xskmapet/bin
workdir=
input=
out=
err=
stoch_io=
nproc=
ppn=

mode_of_op=

export OMP_NUM_THREADS=1
cd $workdir

aprun -n $nproc -N $ppn $execdir/piN2piN_factorized -c $stoch_io -m $mode_of_op -f $input 1>${out}.${PBS_JOBID} 2>${err}.${PBS_JOBID}

es=$?
if [ $es -ne 0 ]; then
  echo "[] Error from aprun, status was $es"
  exit 1
fi

exit 0
