#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

log=$MyName.log

job_list=($(qstat -u $USER | grep "$USER" | awk -F. '{print $1}'))

rm -v ${MyName}.*.log

printf "%2s  %10s %30s %30s %8s %8s %28s %28s %28s\n" "no" "job" "file" "name" "state" "queue"  "created" "last modified" "current queue" | tee $log
printf "=====================================================================================================================================================================================\n" | tee -a $log
i=0
for j in ${job_list[*]}; do
  i=$(($i+1))
  # echo "job $j"
  f=$MyName.$j.log
  qstat -f $j > $f
  n=$( cat $f | awk -F= '/Job_Name\ / {gsub(/\ /,"",$2);print $2}')
  s=$( cat $f | awk -F= '/job_state\ / {gsub(/\ /,"",$2);print $2}')
  q=$( cat $f | awk -F= '/queue\ / {gsub(/\ /,"",$2);print $2}')
  ctime="$( cat $f | awk -F= '/ctime\ / {print $2}')"
  mtime="$( cat $f | awk -F= '/mtime\ / {print $2}')"
  qtime="$( cat $f | awk -F= '/qtime\ / {print $2}')"
  
  printf "%2d  %10s %30s %30s %8s %8s %28s %28s %28s\n" $i $j $f $n $s $q "$ctime" "$mtime" "$qtime"
done 2>&1 | tee -a $log

exit 0
