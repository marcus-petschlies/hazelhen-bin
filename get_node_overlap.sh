#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log

get_nodes_sh=/zhome/academic/HLRS/hsk/xskmapet/bin/get_nodes.sh

Conf=( )

time_stamp=

job_type=

top_level_dir=$PWD

source ${MyName}.in

for d in ${Conf[*]}; do
  cd $top_level_dir/$d
  f=$(find ./ -maxdepth 1 -name piN2piN_factorized.${time_stamp}.${job_type}.out.\*.hazelhen-batch.hww.hlrs.de  | tail -n 1)
  if [ "X$f" == "X" ]; then
    echo "[$MyName] Error, could not find out file for $d $time_stamp $job_type"
    exit 1
  else
    echo "# [$MyName] $d getting node list from file $f"
  fi
  ${get_nodes_sh} $f
  if [ $(wc -l get_nodes.lst | awk '{print $1}') -eq 1 ]; then
    echo "[$MyName] Error, empty node list"
    exit 1
  fi
done 2>&1 | tee -a $log

nconf=$(echo ${Conf[*]} | wc -w)

cd $top_level_dir

for ((i=0; i<$nconf -1; i++ )); do
  d1=${Conf[$i]}
for ((k=$i+1; k<$nconf; k++ )); do
  d2=${Conf[$k]}

  echo "# [$MyName] $d1 $d2"
  for g in $(awk '!/^#/' $d1/get_nodes.lst); do
    awk '$1=='$g'' $d2/get_nodes.lst
  done
done
done 2>&1 | tee -a $log

exit 0
