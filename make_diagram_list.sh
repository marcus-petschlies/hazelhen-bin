#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
log=$MyName.log

sigma_g0d=( 1 -1 -1 -1  1 -1  1 -1 -1 -1  1  1  1 -1 -1 -1 )

########################################
# get irrep dimension from first
#   character of irrep name
########################################

itag=""
source_filename=""

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-t") itag=$2; shift 2;;
      "-s") source_filename=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi


echo "# [$MyName] (`date`)" | tee $log

if [ "X$itag" == "X" ] ; then
  echo "[$MyName] Error, need itag"  | tee -a $log
  exit 1
fi

if [ "X$source_filename" == "X" ] ; then
  source_filename="$MyName.in"
  echo "[$MyName] Warning, no source_filename given, using default $source_filename" | tee -a $log
fi


########################################
########################################
##
## set default values
##
########################################
########################################

########################################
# time extent values
########################################

########################################
# number of sink timeslices
#  ( not including source time )
########################################
src_snk_time_separation=24
TT=96

########################################
########################################
##
## set default values
## for group projection
##
########################################
########################################

########################################
# list of little groups
########################################
littlegroup_list=( 2Oh 2C4v 2C2v 2C3v )

########################################
# total momentum list per little group
########################################
ptot_list=( \
  "0,0,0" \
  "0,0,1" "0,0,-1" "1,0,0" "-1,0,0" "0,1,0" "0,-1,0" \
  "1,1,0" "-1,-1,0" "1,0,1" "-1,0,-1" "0,1,1" "0,-1,-1" "-1,1,0" "1,-1,0" "1,0,-1" "-1,0,1" "0,-1,1" "0,1,-1" \
  "1,1,1" "-1,-1,-1" "1,1,-1" "-1,-1,1" "1,-1,1" "-1,1,-1" "-1,1,1" "1,-1,-1" \
)

fbwd_list=( fwd bwd )
d_spin=4
reorder=0

momentum_square_cut=3

if [ -e $source_filename ]; then
  echo "# [$MyName] source from file $source_filename"  | tee -a $log
  source $source_filename
else
  echo "[$MyName] Warning, nothing to source"  | tee -a $log
fi

########################################
# output filename
########################################
lst=$MyName.${itag}.lst

cat << EOF | tee -a $log
# [$MyName] itag                    = $itag
# [$MyName] source_filename         = $source_filename
# [$MyName] src_snk_time_separation = $src_snk_time_separation
# [$MyName]
# [$MyName] d_spin                  = $d_spin
# [$MyName] reorder                 = $reorder
# [$MyName] fbwd_list               = ${fbwd_list[*]}
# [$MyName] 
# [$MyName] list filename           = $lst
# [$MyName] 
EOF

########################################
# check length of lists for consistency
########################################

num_mf=$( echo ${ptot_list[*]}        | wc -w )
 
########################################
########################################
##
## start the list file
##
########################################
########################################

########################################
# initialize new projector list file
########################################
echo "# [$MyName] (`date`)" > $lst

########################################
#
if [ "X$itag" == "Xm-m" ] || [ "X$itag" == "Xall" ]; then
#
########################################

  tag="m-m"
  type="m-m"

  gi2_list=( 5 )
  gf2_list=( 5 )

  diagrams="m1"
  d_spin=1

  if [ -e $source_filename ]; then
    source $source_filename
  else
    echo "[$MyName] Warning, could not find file $source_filename"
  fi | tee -a $log

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $(( $TT - 1 ))
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0
for((ilg=0; ilg<$num_mf; ilg++)); do

  nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
  echo "# [$MyName] nptot = $nptot" | tee -a  $log

  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

    ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )
    if [ "X$ptot" == "X" ]; then
      echo "[$MyName] Error, ptot is empty"  | tee -a $log
      exit 5
    fi

    pf2=$ptot
    pi2=$(echo $ptot | tr ',' ' ' | awk '{printf("%d,%d,%d", -$1, -$2, -$3)}')
 
    echo "# [$MyName] $ilg ptot = $ptot pi2 = $pi2 pf2 = $pf2" | tee -a $log

    for fbwd in ${fbwd_list[*]}; do

      for gi2 in ${gi2_list[*]}; do
      for gf2 in ${gf2_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  pi2      = $pi2
  pf2      = $pf2
  gi2      = $gi2
  gf2      = $gf2
  fbwd     = $fbwd
EndTwopointFunction

EOF
          counter=$(( $counter + 1 ))
        done  # of gf2
        done  # of gi2
      done  # of fbwd
  done  # of ptot in class of moving frame
done  # of ilg

echo "# [$MyName] number of $tag diagram entries $counter"

fi

########################################
########################################

########################################
if [ "X$itag" == "XN-N" ] || [ "X$itag" == "Xall" ]; then
########################################

  tag="N-N"
  type="b-b"

  gi1_list=(  14,4  11,5  8,4 )
  gf1_list=(  14,4  11,5  8,4 )

  diagrams="n1,n2"

  if [ -e $source_filename ]; then
    source $source_filename
  else
    echo "[$MyName] Warning, could not find file $source_filename"
  fi | tee -a $log

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0
for((ilg=0; ilg<$num_mf; ilg++)); do

  nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
  echo "# [$MyName] nptot = $nptot" | tee -a  $log

  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

    ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )
    if [ "X$ptot" == "X" ]; then
      echo "[$MyName] Error, ptot is empty"  | tee -a $log
      exit 5
    fi

    pf1=$ptot
    pi1=$(echo $ptot | tr ',' ' ' | awk '{printf("%d,%d,%d", -$1, -$2, -$3)}')
 
    echo "# [$MyName] $ilg ptot = $ptot pi1 = $pi1 pf1 = $pf1" | tee -a $log

    for fbwd in ${fbwd_list[*]}; do

      for gi1 in ${gi1_list[*]}; do
      for gf1 in ${gf1_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  pi1      = $pi1
  pf1      = $pf1
  gi1      = $gi1
  gf1      = $gf1
  fbwd     = $fbwd
EndTwopointFunction

EOF
          counter=$(( $counter + 1 ))
        done  # of gf1
        done  # of gi1
      done  # of fbwd
  done  # of ptot in class of moving frame
done  # of ilg

echo "# [$MyName] number of $tag diagram entries $counter"

fi

########################################
########################################


########################################
if [ "X$itag" == "XD-D" ] || [ "X$itag" == "Xall" ];
then
########################################

  tag="D-D"
  type="b-b"

  gi1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="d1,d2,d3,d4,d5,d6"

  ########################################
  # source input file
  ########################################
  if [ -e $source_filename ]; then
    source $source_filename
  else
    echo "[$MyName] Warning, could not find file $source_filename" | tee -a $log 
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

counter=0
for((ilg=0; ilg<$num_mf; ilg++)); do

  nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
  echo "# [$MyName] $ilg nptot = $nptot" | tee -a $log

  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

    ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )
    if [ "X$ptot" == "X" ]; then
      echo "[$MyName] Error, ptot is empty"  | tee -a $log
      exit 5
    fi

    pf1=$ptot
    pi1=$(echo $ptot | tr ',' ' ' | awk '{printf("%d,%d,%d", -$1, -$2, -$3)}')
 
    echo "# [$MyName] $ilg ptot = $ptot pi1 = $pi1 pf1 = $pf1" | tee -a $log

    for fbwd in ${fbwd_list[*]}; do

      for gi1 in ${gi1_list[*]}; do
          
        gi1_12=($( echo $gi1 | tr ',' ' ' ))

        norm_str="  # norm     ="
        s0d=${sigma_g0d[${gi1_12[0]}]}
        if [ $s0d -ne 1 ]; then
          norm_str="  norm     = $s0d"
          for((i=1; i<$num_diag; i++));do norm_str="${norm_str},${sigma_g0d[${gi1_12[0]}]}"; done
        fi

        for gf1 in ${gf1_list[*]}; do

          gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF >> $lst

BeginTwopointFunctionInit
  pi1      = $pi1
  pf1      = $pf1
  gi1      = $gi1
  gf1      = $gf1
  fbwd     = $fbwd
$norm_str
EndTwopointFunction

EOF
          counter=$(( $counter + 1 ))

        done  # of gf1
      done  # of gi1
    done  # of fbwd
  done  # of ptot
done  # of ilg

echo "# [$MyName] number of $tag diagram entries $counter"
fi
########################################
########################################

########################################
if [ "X$itag" == "XpixN-D" ] || [ "X$itag" == "Xall" ]
then
########################################

  tag="pixN-D"
  type="mxb-b"

###  pi2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"
  
  p2_list=( "0,0,0" "0,0,1" "0,0,-1" "1,0,0" "-1,0,0" "0,1,0" "0,-1,0" "1,1,0" "-1,-1,0" "1,0,1" "-1,0,-1" "0,1,1" "0,-1,-1" "-1,1,0" "1,-1,0" "1,0,-1" "-1,0,1" "0,-1,1" "0,1,-1" "1,1,1" "-1,-1,-1" "1,1,-1" "-1,-1,1" "1,-1,1" "-1,1,-1" "-1,1,1" "1,-1,-1" )


  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="t1,t2,t3,t4,t5,t6"

  source $source_filename

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" >> $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

  counter=0

  nptot=$( echo ${ptot_list[*]} | wc -w )
  echo "# [$MyName] nptot = $nptot" >> $log

  # loop on ptot = total momentum = pf1
  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

      ptot=${ptot_list[$iptot]}

      ptot_vec=($( echo $ptot | tr ',' ' ' ))
      pf1=$ptot

      # momentum_pairs_filename="momentum_pairs-PX${ptot_vec[0]}_PY${ptot_vec[1]}_PZ${ptot_vec[2]}.lst"
      # echo "# [$MyName] momentum_pairs_filename = $momentum_pairs_filename"
      # source $momentum_pairs_filename


      # loop on pi
      num_pi=$( echo ${p2_list[*]} | wc -w )
      for(( ipi=0; ipi<$num_pi; ipi++ )); 
      do
        pi2_vec=( $( echo ${p2_list[$ipi]} | awk -F, '{print $1, $2, $3}' ) )
        pi2="${pi2_vec[0]},${pi2_vec[1]},${pi2_vec[2]}"

        # pi1_vec=( $( echo ${p1_list[$ipi]} | awk -F, '{print -$1, -$2, -$3}' ) )
        pi1_vec=( $(( -${ptot_vec[0]} -${p2_vec[0]})) $(( -${ptot_vec[1]} -${p2_vec[1]})) $(( -${ptot_vec[2]} -${p2_vec[2]})))
        pi1="${pi1_vec[0]},${pi1_vec[1]},${pi1_vec[2]}"
        
        echo "# [$MyName] ptot = $ptot pi1 = $pi1  pi2 = $pi2  pf1 = $pf1" >> $log

        # loop on forward, backward
        for fbwd in ${fbwd_list[*]}; do

          # loop on gamma structures
          for gi1 in ${gi1_list[*]}; do
          
            gi1_12=($( echo $gi1 | tr ',' ' ' ))

          for gi2 in ${gi2_list[*]}; do

          for gf1 in ${gf1_list[*]}; do

            gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF >> $lst

BeginTwopointFunctionInit
  pi1      = $pi1
  pi2      = $pi2
  pf1      = $pf1
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  fbwd     = $fbwd
EndTwopointFunction

EOF

            counter=$(( $counter + 1 ))
          done  # of gf1
          done  # of gi2
          done  # of gi1
        done  # of fbwd
      done  # of pi
  done  # of ptot

  echo "# [$MyName] number of $tag diagram entries $counter"
fi
########################################
########################################

########################################
if [ "X$itag" == "XpixN-pixN" ] || [ "X$itag" == "Xall" ]
then
########################################

  tag="pixN-pixN"
  type="mxb-mxb"

#  pi2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"
#  pf2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"

  p1_list=( "0,0,0" "0,0,1" "0,0,-1" "1,0,0" "-1,0,0" "0,1,0" "0,-1,0" "1,1,0" "-1,-1,0" "1,0,1" "-1,0,-1" "0,1,1" "0,-1,-1" "-1,1,0" "1,-1,0" "1,0,-1" "-1,0,1" "0,-1,1" "0,1,-1" "1,1,1" "-1,-1,-1" "1,1,-1" "-1,-1,1" "1,-1,1" "-1,1,-1" "-1,1,1" "1,-1,-1" )

  p2_list=( "0,0,0" "0,0,1" "0,0,-1" "1,0,0" "-1,0,0" "0,1,0" "0,-1,0" "1,1,0" "-1,-1,0" "1,0,1" "-1,0,-1" "0,1,1" "0,-1,-1" "-1,1,0" "1,-1,0" "1,0,-1" "-1,0,1" "0,-1,1" "0,1,-1" "1,1,1" "-1,-1,-1" "1,1,-1" "-1,-1,1" "1,-1,1" "-1,1,-1" "-1,1,1" "1,-1,-1" )


  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=( 14,4  11,5  8,4 )
  gf2_list=(  5 )

  diagrams="b1,b2,w1,w2,w3,w4,z1,z2,z3,z4,s1,s2"

  source $source_filename

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" >> $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

  counter=0

  nptot=$( echo ${ptot_list[*]} | wc -w )
  echo "# [$MyName] nptot = $nptot" >> $log

  # loop on ptot = total momentum = pf1
  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

      ptot=$( echo ${ptot_list[$iptot]} )

      ptot_vec=($( echo $ptot | tr ',' ' ' ))

      momentum_pairs_filename="momentum_pairs-PX${ptot_vec[0]}_PY${ptot_vec[1]}_PZ${ptot_vec[2]}.lst"
      echo "# [$MyName] momentum_pairs_filename = $momentum_pairs_filename"
      source $momentum_pairs_filename

      # loop on pf1,2
      num_pf=$( echo ${p2_list[*]} | wc -w )
      for(( ipf=0; ipf<$num_pf; ipf++ ));
      do
        pf2=${p2_list[$ipf]}

        pf1=${p1_list[$ipf]}

      # loop on pi1,2
      num_pi=$num_pf
      for(( ipi=0; ipi<$num_pi; ipi++ )); 
      do
        pi2_vec=( $( echo ${p2_list[$ipi]} | awk -F, '{print -$1, -$2, -$3}' ) )
        pi2="${pi2_vec[0]},${pi2_vec[1]},${pi2_vec[2]}"

        pi1_vec=( $( echo ${p1_list[$ipi]} | awk -F, '{print -$1, -$2, -$3}' ) )
        pi1="${pi1_vec[0]},${pi1_vec[1]},${pi1_vec[2]}"

        echo "# [$MyName] ptot = $ptot   pi1 = $pi1   pi2 = $pi2   pf1 = $pf1   pf2 = $pf2" >> $log

        # loop on forward, backward
        for fbwd in ${fbwd_list[*]}; do

 
          # loop on gamma structures
          for gi1 in ${gi1_list[*]}; do
          
            gi1_12=($( echo $gi1 | tr ',' ' ' ))

          for gi2 in ${gi2_list[*]}; do

          for gf1 in ${gf1_list[*]}; do

            gf1_12=($( echo $gf1 | tr ',' ' ' ))

          for gf2 in ${gf2_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  pi1      = $pi1
  pi2      = $pi2
  pf1      = $pf1
  pf2      = $pf2
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  gf2      = $gf2
  fbwd     = $fbwd
EndTwopointFunction

EOF
            counter=$(( $counter + 1 ))

          done  # of gf2
          done  # of gf1
          done  # of gi2
          done  # of gi1
        done  # of fbwd
      done  # of pi2 
      done  # of pf2
  done  # of ptot

  echo "# [$MyName] number of $tag diagram entries $counter"

fi

#else
#  echo "[$MyName] Error, unrecognized tag" | tee -a $log
#  exit 11
#fi

echo "# [$MyName] (`date`)" >> $lst

echo "# [$MyName] (`date`)" | tee -a $log
exit 0
