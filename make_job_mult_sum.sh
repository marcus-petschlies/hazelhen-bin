#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

Conf=( )

work_path=$PWD

source_coords_filename=
source_coords_select=( 1 0 )

with_submit=no
ensemble_name=
run_prefix="diagram_sum"
src_per_conf=
src_per_job=
walltime=24:00:00
data_file_path=

execdir=
execname=
mymail=
ppn=24
nodes_per_call=1
nodes_total=
twopoint_function_tag=all

date_tag_fix=0
date_tag=

n_coherent_source=2

# lg_file=$work_path/little_groups_2Oh.tab
lg_file=NA


LL=
TT=
JOB_LOG=$work_path/JOB_LOG
echo "# [$MyName] JOB_LOG = $JOB_LOG"

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-L") LL=$2; shift 2;;
      "-T") TT=$2; shift 2;;
      "-i") cvc_input_mask=$2; shift 2;;
      "-j") job_mask=$2; shift 2;;
      "-s") source_coords_filename=$2; shift 2;;
      "-m") src_per_job=$2; shift 2;;
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      "-w") walltime=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-p") data_file_path=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

source $MyName.in

      job_mask=$work_path/${run_prefix}.sh
cvc_input_mask=$work_path/${run_prefix}.input

if [ "X$src_per_job" == "X" ] || [ "X$src_per_conf" == "X" ]; then
  echo "[$MyName] need number of src_per_conf and src_per_job"
  exit 1
fi

if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "[$MyName] Error, need L and T"
  exit 2
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "[$MyName] Error, need ensemble name"
  exit 3
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "[$MyName] Error, need source coords filename"
  exit 4
fi

if [ "X$data_file_path" == "X" ] ; then
  echo "[$MyName] Error, need data file path"
  exit 5
fi

if [ "X$nodes_per_call" == "X" ] || [ "X$ppn" == "X" ]; then
  echo "[$MyName] Error, need nodes_per_call and ppn"
  exit 6
fi

if [ "X$execdir" == "X" ] || [ "X$execname" == "X" ]; then
  echo "[$MyName] Error, need execdir and execname"
  exit 6
fi

if [ "X$twopoint_function_tag" == "X" ]; then
  echo "[$MyName] Error, need twopoint_function_tag"
  exit 7
fi

if ![ -e $lg_file ]; then
  echo "[$MyName] Error, little gropu file $lg_file not found"
  exit 8
fi

cat << EOF

###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit          = $with_submit
# [$MyName] ensemble name        = $ensemble_name
# [$MyName] work_path            = $work_path
# [$MyName] data_file_path       = $data_file_path
# [$MyName]
# [$MyName] L                    = $LL
# [$MyName] T                    = $TT
# [$MyName]
# [$MyName] source coords file   = $source_coords_filename
# [$MyName] source coords select = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] input mask           = $cvc_input_mask
# [$MyName] total_momentum_list  = ${total_momentum_list[*]}
# [$MyName]
# [$MyName] n_coherent_source    = $n_coherent_source
# [$MyName] src_per_conf         = $src_per_conf
# [$MyName] src_per_job          = $src_per_job
# [$MyName] walltime             = $walltime
# [$MyName]
# [$MyName] lg_file              = $lg_file
# [$MyName]
# [$MyName] nodes_per_call       = $nodes_per_call
# [$MyName] ppn                  = $ppn
# [$MyName] execdir              = $execdir
# [$MyName] execname             = $execname
# [$MyName] mymail               = $mymail
# [$MyName]
# [$MyName] date_tag_fix         = $date_tag_fix
# [$MyName]
# [$MyName] twopoint_function_tag = $twopoint_function_tag
# [$MyName]
EOF

sleep 1s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

###########################################################
# generate projector list
###########################################################
if ! [ -e ${work_path}/make_diagram_sum_list.sh ] && ! [ -L ${work_path}/make_diagram_sum_list.sh ]
then
  echo "[$MyName] Error, no make_diagram_sum_list found"
  exit 1
fi
${work_path}/make_diagram_sum_list.sh -t $twopoint_function_tag

###########################################################
# loop on gauge config charges
###########################################################

awk '/^Conf=/ {print $0 }' ${MyName}.in |
while read -r line; do

  eval $line

  nconf=$(echo ${Conf[*]} | wc -w)

  nodes_total=$(( $nodes_per_call * ( $src_per_conf * nconf / $src_per_job ) ))

  if [ $date_tag_fix -eq 0 ]; then
    date_tag=`date +%Y_%m_%d_%H_%M_%S`
  fi

  job_tag=${date_tag}

  job=${work_path}/${run_prefix}.${job_tag}.sh

cat << EOF 

# [$MyName]   date_tag               = $date_tag
# [$MyName]
# [$MyName]   Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName]   nconf                  = $nconf
# [$MyName]   nodes_total            = $nodes_total
# [$MyName]   job                    = $job
# [$MyName]
EOF

sleep 1s

#PBS -l nodes=${nodes_total}:ppn=${ppn}
#PBS -l select=${nodes_total}:node_type=rome:mpiprocs=1

cat << EOF > $job 
#!/bin/bash
#PBS -N ${ensemble_name}_${run_prefix}_${job_tag}
#PBS -l select=${nodes_total}:node_type=rome:ompthreads=${ppn}
#PBS -l walltime=${walltime}

#PBS -M ${mymail}
#PBS -m abe

source $module_file

echo "# [\$PBS_JOBID] (\`date\`) start of run"

export OMP_NUM_THREADS=$ppn

execdir=$execdir
execname=$execname

EOF

#################################################
# loop on groups of configurations
#################################################
for g in ${Conf[*]}; do

  gfmt=$(printf "%.4d" $g)

  WDIR=$work_path/$g
  if ! [ -d $WDIR ]; then
    echo "[$MyName] Error, work dir $WDIR does not exist"
    exit 10
  fi
  cd $WDIR || exit 11

  source_coords_list=($(awk '
      $1=='$g' && ($2%'${TT}')<'$TT'/'$n_coherent_source' {
        printf("%d,%d,%d,%d\n", ($2%'${TT}'), ($3%'${LL}'), ($4%'${LL}'), ($5%'${LL}') )
      }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))

  source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
  if [ $source_coords_number -ne $src_per_conf ]; then
    echo "[$MyName] Error, $g incompatible source_coords_number $source_coords_number vs. $src_per_conf"
    exit 14
  fi

  njob=$(( ( $source_coords_number + $src_per_job -1 ) / $src_per_job ))

  if [ "X${lg_file}X" != "XNAX" ]; then
    ln -sf $lg_file .
  fi

cat << EOF
# [$MyName]     conf                 = $g
# [$MyName]     source_coords_number = $source_coords_number
# [$MyName]     njob                 = $njob
# [$MyName]
EOF

  #################################################
  # loop on jobs per configuration
  #################################################
  for (( i=0; i<$njob; i++)); do

    cvc_input=${run_prefix}.${date_tag}.${i}.input
    out=${run_prefix}.${date_tag}.${i}.out
    err=${run_prefix}.${date_tag}.${i}.err

cat << EOF >> $job
  
cd $WDIR
input=$cvc_input
out=$out
err=$err

### aprun -n $nodes_per_call \$execdir/\$execname -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &
### mpirun -np $nodes_per_call omplace -nt $ppn 

\$execdir/\$execname -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &

EOF

    ###########################################################
    # cvc input file
    ###########################################################
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/ {print "Nconf = '$g'"; next}
      /^filename_prefix[\ =]/ {print "filename_prefix = '"$data_file_path/$g/piN_piN_diagrams"'"; next}
      /^num_threads[\ =]/ {print "num_threads = '$ppn'"; next}
      /^coherent_source_number[\ =]/ { print "coherent_source_number = '"$n_coherent_source"'"; next}
      /^T[\ =]/                      { print "T  = '$TT'"; next}
      /^LX[\ =]/                     { print "LX = '$LL'"; next}
      /^LY[\ =]/                     { print "LY = '$LL'"; next}
      /^LZ[\ =]/                     { print "LZ = '$LL'"; next}
      {print}' > $cvc_input
     
    for total_momentum in ${total_momentum_list[*]}; do
      echo "total_momentum = ${total_momentum}"
    done >> $cvc_input

    ###########################################################
    # add source coords for this call
    ###########################################################
    for (( isrc=$(( $i * $src_per_job )); isrc<$(( ($i + 1) * $src_per_job )); isrc++)); do
      if [ "X${source_coords_list[$isrc]}" != "X" ]; then
        printf "source_coords = %s\n" ${source_coords_list[$isrc]}
      fi
    done  >> $cvc_input

    ###########################################################
    # add twopoint_function list to cvc input file
    ###########################################################
    if ! [ -e ${work_path}/make_diagram_sum_list.${twopoint_function_tag}.lst ]; then
      echo "[$MyName] Error, could not find file ${work_path}/make_diagram_sum_list.${twopoint_function_tag}.lst"
      exit 122
    else 
      cat ${work_path}/make_diagram_sum_list.${twopoint_function_tag}.lst >> $cvc_input
    fi

  done  # end of loop on jobs
done  # end of loop on configs

cat << EOF >> $job

wait

echo "# [\$PBS_JOBID] (\`date\`) end of run"
exit 0

EOF

if [ "X$with_submit" == "Xyes" ]; then
  echo "# [$MyName] (`date`) $job" 
  qsub $job

  sleep 2s
fi 2>&1 | tee -a $JOB_LOG

done  # end of while on conf charges

echo "# [$MyName] (`date`)"
exit 0
