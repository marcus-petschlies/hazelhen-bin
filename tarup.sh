#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

top_level_dir=$PWD
untar="false"

prefix="piN_piN_diagrams"

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-u") untar=true; shift 1;;
      *) exit 1;;
    esac
  done
fi

echo "# [$MyName] untar = $untar"

Conf=( 356 360 364 368 372 376 380 384 388 392 448 452 456 460 468 472 476 480 484 488 492 496 520 524 528 532 536 540 544 548 )

for d in ${Conf[*]}; do

  dfmt=`printf "%.4d" $d`

  cd $top_level_dir/$d || exit 1

  if [ "X$untar" == "Xfalse" ]; then
 
     echo "# [$MyName] (`date`)" begin tar for $d

    tar -cf ${prefix}.tar ${prefix}.*.$dfmt.*.aff 
    es=$?
    if [ $es -ne 0 ]; then
      echo "[$MyName] Error from tar; status was $es"
      exit 2
    else
      rm -v ${prefix}.*.${dfmt}.*.aff
    fi 

     echo "# [$MyName] (`date`)" end   tar for $d
  elif [ "X$untar" == "Xtrue" ]; then

    echo "# [$MyName] (`date`)" begin untar for $d

    tar -xf ${prefix}.tar

    es=$?
    if [ $es -ne 0 ]; then
      echo "[$MyName] Error from tar; status was $es"
      exit 3
#    else
#      rm -v ${prefix}.tar
    fi 

    echo "# [$MyName] (`date`)" end   untar for $d
  fi

done

exit 0
