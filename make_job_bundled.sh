#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

Conf=( )

work_path=$PWD

source_coords_filename=

stoch_production=
invcon_production=

stoch_path=

with_submit=no
n_coherent_source=2
LL=
TT=
ensemble_name=

walltime_prod=24:00:00
walltime_stoch=24:00:00
nodes_per_call=
ppn=24
nproc_per_call=

run_prefix="piN2piN_factorized"
JOB_LOG=$work_path/JOB_LOG

execdir=/zhome/academic/HLRS/hsk/xskmapet/bin
answer_wait_time=1

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-L") LL=$2; shift 2;;
      "-T") TT=$2; shift 2;;
      "-c") checkedInput=$2; shift 2;;
      "-n") n_coherent_source=$2; shift 2;;
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      "-W") walltime_prod=$2; shift 2;;
      "-w") walltime_stoch=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-f") stoch_production=$2; shift 2;;
      "-F") invcon_production=$2; shift 2;;
      "-Nn") nodes_per_call=$2; shift 2;;
      "-Np") nproc_per_call=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

source ${MyName}.in

         job_mask=$work_path/job.sh
   cvc_input_mask=$work_path/cvc.input
tmLQCD_input_mask=$work_path/invert.input


if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Error, need source coords filename"
  exit 1
fi

if 
[ "X$nodes_per_call" == "X" ] || [ "X$nproc_per_call" == "X" ] || [ "X$ppn" == "X" ] ; then
  echo "# [$MyName] Error, number of nodes_per_call, nproc_per_call, ppn"
  exit 1
fi

nconf=$(echo ${Conf[*]} | wc -w)
nproc_total=$(( $nconf * $nproc_per_call ))
nodes_total=$(( ( $nproc_total + $ppn - 1 ) / $ppn  ))

date_tag=`date +%Y_%m_%d_%H_%M_%S`

cat << EOF
###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit            = $with_submit
# [$MyName] ensemble name          = $ensemble_name
# [$MyName] work_path              = $work_path
# [$MyName]
# [$MyName] L                      = $LL
# [$MyName] T                      = $TT
# [$MyName] source coords file     = $source_coords_filename
# [$MyName]
# [$MyName] job mask               = $job_mask
# [$MyName] date_tag               = $date_tag
# [$MyName]
# [$MyName] input mask             = $cvc_input_mask
# [$MyName] tmLQCD_input_mask      = $tmLQCD_input_mask
# [$MyName]
# [$MyName] Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName] n_coherent source      = $n_coherent_source
# [$MyName]
# [$MyName] stoch_production       = $stoch_production
# [$MyName] invcon_production      = $invcon_production
# [$MyName]
# [$MyName] walltime_prod          = $walltime_prod
# [$MyName] walltime_stoch         = $walltime_stoch
# [$MyName] nodes_per_call         = $nodes_per_call
# [$MyName] ppn                    = $ppn
# [$MyName] nproc_per_call         = $nproc_per_call
# [$MyName]
# [$MyName] nproc_total            = $nproc_total
# [$MyName] nodes_total            = $nodes_total

EOF

sleep ${answer_wait_time}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

job_id=""
###########################################################
# stochastic propagator job
###########################################################
if [ "X$stoch_production" == "Xyes" ]; then

  job_tag=${date_tag}.0

           job=${run_prefix}.${job_tag}.sh
     cvc_input=${run_prefix}.${job_tag}.input
  tmLQCD_input=invert.input
           out=${run_prefix}.${job_tag}.out
           err=${run_prefix}.${job_tag}.err
  stoch_io="-w -W"
  mode=1
  

cat << EOF > $job
#!/bin/bash
#PBS -N ${ensemble_name}_stoch_${job_tag}
#PBS -l nodes=${nodes_total}:ppn=$ppn
#PBS -l walltime=${walltime_stoch}
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe

echo "# (\$(date))"

export OMP_NUM_THREADS=1

execdir=$execdir
stoch_io="$stoch_io"
mode_of_op=$mode
nproc_per_call=$nproc_per_call
nodes_per_call=$nodes_per_call
ppn=$ppn

EOF

  #  loop on gauge configurations
  for g in ${Conf[*]}; do

    gfmt=$(printf "%.4d" $g)
    WDIR=$work_path/$g
    mkdir -p $WDIR 

    source_coords_list=($(awk '$1=='$g' && $2<'$TT'/'$n_coherent_source' {printf("%d,%d,%d,%d ", $2,$3,$4,$5)}' $source_coords_filename))
    source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
    echo "# [$MyName] ($g) source coords number = $source_coords_number"

cat << EOF >> $job
workdir=$WDIR
input=$cvc_input
out=$out
err=$err

cd \$workdir
aprun -n \$nproc_per_call -N \$ppn \$execdir/piN2piN_factorized -c \$stoch_io -m \$mode_of_op -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &

EOF
 
    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '$g'"; next}
      /^seed[\ =]/            { print "seed = "(1000000+'$g'); next}
      /^filename_prefix[\ =]/  { print "filename_prefix  = source"; next}
      /^filename_prefix2[\ =]/ { print "filename_prefix2 = prop"; next}
      {print}' > $WDIR/$cvc_input

    # append source coords list
    for(( isrc=0; isrc<$source_coords_number; isrc++)); do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]} >> $WDIR/$cvc_input
    done

    cat $tmLQCD_input_mask | awk '
      /^InitialStoreCounter[\ =]/ {print "InitialStoreCounter = '$g'"; next}
      {print}' > $WDIR/$tmLQCD_input

  done  # of loop on gauge configurations

cat << EOF >> $job

wait

echo "# (\$(date))"
exit 0
EOF
fi  # of if stoch job

###########################################################
# inversion+contraction job
###########################################################
if [ "X$invcon_production" == "Xyes" ]; then

  job_tag=${date_tag}.1
  job=${run_prefix}.${job_tag}.sh

  stoch_io="-r -R"
  mode=7

cat << EOF > $job 
#!/bin/bash
#PBS -N ${ensemble_name}_prod_$job_tag
#PBS -l walltime=$walltime_prod
#PBS -l nodes=$nodes_total:ppn=$ppn
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe

export OMP_NUM_THREADS=1

execdir=$execdir
mode_of_op=${mode}
nproc_per_call=$nproc_per_call
nodes_per_call=$nodes_per_call
ppn=$ppn
EOF

  #  loop on gauge configurations
  for g in ${Conf[*]}; do

    gfmt=$(printf "%.4d" $g)

    WDIR=$work_path/$g
    mkdir -p $WDIR 

    source_coords_list=($(awk '$1=='$g' && $2<'$TT'/'$n_coherent_source' {printf("%d,%d,%d,%d ", $2,$3,$4,$5)}' $source_coords_filename))
    source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
    echo "# [$MyName] source coords number = $source_coords_number"

       cvc_input=${run_prefix}.${job_tag}.input
    tmLQCD_input=invert.input
             out=${run_prefix}.${job_tag}.out
             err=${run_prefix}.${job_tag}.err
   
cat << EOF >> $job

workdir=$WDIR
input=$cvc_input
out=$out
err=$err
stoch_io="$stoch_io"

cd \$workdir
aprun -n \$nproc_per_call -N \$ppn \$execdir/piN2piN_factorized -c \$stoch_io -m \$mode_of_op -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &

EOF

    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '$g'"; next}
      /^seed[\ =]/            { print "seed = "(1000000+'$g'); next}
      /^filename_prefix[\ =]/  { print "filename_prefix = '"$stoch_path"'/'$g'/source"; next}
      /^filename_prefix2[\ =]/ { print "filename_prefix2 = '"$stoch_path"'/'$g'/prop"; next}
      {print}' > $WDIR/$cvc_input

    # append source coords list
    for (( isrc=0; isrc<$source_coords_number; isrc++)); do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]}
    done >> $WDIR/$cvc_input

    cat $tmLQCD_input_mask | awk '
      /^InitialStoreCounter[\ =]/ {print "InitialStoreCounter = '$g'"; next}
      {print}' > $WDIR/$tmLQCD_input

  done  # end of loop on configurations
   
cat << EOF >> $job

echo "# (\$(date))"

wait

exit 0
EOF

  if [ "X$with_submit" == "Xyes" ]; then

    echo "# [$MyName] (`date`) $job_tag " 2>&1 | tee -a $JOB_LOG
      
    if [ "X$job_id" != "X" ]; then
      echo "# [$MyName] depend on jobid = $jobid"
      qsub -W depend=afterok:$jobid $job 2>&1 | tee -a $JOB_LOG
    else
      qsub $job 2>&1 | tee -a $JOB_LOG
    fi

    sleep 3s
  fi  # end of if with submit

fi  # of if invcon job

echo "# [$MyName] (`date`)"
exit 0
