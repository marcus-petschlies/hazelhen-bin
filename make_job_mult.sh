#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

# Conf=( )

work_path=

source_coords_filename=
source_coords_select=( 1 0 )

stoch_production=
invcon_production=

stoch_mode=1
invcon_mode=7

stoch_path=".."

with_submit=no
n_coherent_source=2
LL=
TT=
ensemble_name=

walltime_prod=24:00:00
walltime_stoch=24:00:00
### nodes_per_call=
ppn=24
nproc_per_call=

run_prefix="piN2piN_factorized"

execdir=/zhome/academic/HLRS/hsk/xskmapet/bin
answer_wait_time=1
seed_offset=1000000

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-L") LL=$2; shift 2;;
      "-T") TT=$2; shift 2;;
      "-c") checkedInput=$2; shift 2;;
      "-n") n_coherent_source=$2; shift 2;;
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      "-W") walltime_prod=$2; shift 2;;
      "-w") walltime_stoch=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-f") stoch_production=$2; shift 2;;
      "-F") invcon_production=$2; shift 2;;
###      "-Nn") nodes_per_call=$2; shift 2;;
      "-Np") nproc_per_call=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

source ${MyName}.in

if [ "X$work_path" == "X" ]; then
  echo "[$MyName] Error, need work_path ( -d <work_path> )"
  exit 1
fi

          JOB_LOG=$work_path/JOB_LOG
         job_mask=$work_path/job.sh
   cvc_input_mask=$work_path/cvc.input
tmLQCD_input_mask=$work_path/invert.input


if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Error, need source coords filename"
  exit 1
fi

if [ "X$nproc_per_call" == "X" ] || [ "X$ppn" == "X" ] ; then
  echo "# [$MyName] Error, need numbers nproc_per_call and ppn"
  exit 1
fi
# [ "X$nodes_per_call" == "X" ] ||  nodes_per_call

if [ "X$invcon_mode" == "X" ] && [ "X$invcon_production" == "Xyes" ] ; then
  echo "# [$MyName] Error, need mode invcon_mode for invcon_production"
  exit 1
fi

if [ "X$stoch_mode" == "X" ] && [ "X$stoch_production" == "Xyes" ] ; then
  echo "# [$MyName] Error, need mode stoch_mode for stoch_production"
  exit 1
fi

cat << EOF
###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit            = $with_submit
# [$MyName] ensemble name          = $ensemble_name
# [$MyName] work_path              = $work_path
# [$MyName]
# [$MyName] L                      = $LL
# [$MyName] T                      = $TT
# [$MyName]
# [$MyName] source coords file     = $source_coords_filename
# [$MyName] source coords select   = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] job mask               = $job_mask
# [$MyName]
# [$MyName] input mask             = $cvc_input_mask
# [$MyName] tmLQCD_input_mask      = $tmLQCD_input_mask
# [$MyName]
# [$MyName] n_coherent source      = $n_coherent_source
# [$MyName]
# [$MyName] stoch_production       = $stoch_production
# [$MyName] invcon_production      = $invcon_production
# [$MyName]
# [$MyName] stoch_mode             = $stoch_mode
# [$MyName] invcon_mode            = $invcon_mode
# [$MyName]
# [$MyName] stoch_path             = $stoch_path
# [$MyName]
# [$MyName] walltime_prod          = $walltime_prod
# [$MyName] walltime_stoch         = $walltime_stoch

# [$MyName] ppn                    = $ppn
# [$MyName] nproc_per_call         = $nproc_per_call
# [$MyName]
# [$MyName] seed_offset            = $seed_offset
# [$MyName]

EOF
# [$MyName] nodes_per_call         = $nodes_per_call

sleep ${answer_wait_time}s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi

###########################################################
# loop on gauge config charges
###########################################################

awk '/^Conf=/ {print $0 }' ${MyName}.in |
while read -r line; do

  eval $line

  nconf=$(echo ${Conf[*]} | wc -w)

  nproc_total=$(( $nconf * $nproc_per_call ))
  nodes_total=$(( ( $nproc_total + $ppn - 1 ) / $ppn  ))

  date_tag=`date +%Y_%m_%d_%H_%M_%S`

cat << EOF 

# [$MyName] date_tag               = $date_tag
# [$MyName]
# [$MyName] Conf                   = ${Conf[*]}
# [$MyName]
# [$MyName] nconf                  = $nconf
# [$MyName] nproc_total            = $nproc_total
# [$MyName] nodes_total            = $nodes_total
EOF

sleep 1s

jobid=""
###########################################################
# stochastic propagator job
###########################################################
if [ "X$stoch_production" == "Xyes" ]; then

  job_tag=${date_tag}.0

           job=${run_prefix}.${job_tag}.sh
     cvc_input=${run_prefix}.${job_tag}.input
  tmLQCD_input=invert.input
           out=${run_prefix}.${job_tag}.out
           err=${run_prefix}.${job_tag}.err
  stoch_io="-w -W"
  mymode=${stoch_mode}
  

cat << EOF > $job
#!/bin/bash
#PBS -N ${ensemble_name}_stoch_${job_tag}
#PBS -l nodes=${nodes_total}:ppn=$ppn
#PBS -l walltime=${walltime_stoch}
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe

echo "# (\$(date))"

export OMP_NUM_THREADS=1

execdir=$execdir
stoch_io="$stoch_io"
mode_of_op=$mymode
nproc_per_call=$nproc_per_call

ppn=$ppn

EOF
# nodes_per_call=$nodes_per_call

  #  loop on gauge configurations
  for g in ${Conf[*]}; do

    gfmt=$(printf "%.4d" $g)
    WDIR=$work_path/$g
    mkdir -p $WDIR 

    source_coords_list=($(awk '
        $1=='$g' && $2<'$TT'/'$n_coherent_source' {
          printf("%d,%d,%d,%d\n", $2,$3,$4,$5)
        }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))
    source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
    echo "# [$MyName] ($g) source coords number = $source_coords_number"

cat << EOF >> $job
workdir=$WDIR
input=$cvc_input
out=$out
err=$err

cd \$workdir
aprun -n \$nproc_per_call -N \$ppn \$execdir/piN2piN_factorized -c \$stoch_io -m \$mode_of_op -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &

EOF
 
    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '$g'"; next}
      /^seed[\ =]/            { print "seed = "('${seed_offset}'+'$g'); next}
      /^filename_prefix[\ =]/  { print "filename_prefix  = source"; next}
      /^filename_prefix2[\ =]/ { print "filename_prefix2 = prop"; next}
      {print}' > $WDIR/$cvc_input

    # append source coords list
    for(( isrc=0; isrc<$source_coords_number; isrc++)); do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]} >> $WDIR/$cvc_input
    done

    cat $tmLQCD_input_mask | awk '
      /^InitialStoreCounter[\ =]/ {print "InitialStoreCounter = '$g'"; next}
      {print}' > $WDIR/$tmLQCD_input

  done  # of loop on gauge configurations

cat << EOF >> $job

wait

echo "# (\$(date))"
exit 0
EOF

  if [ "X$with_submit" == "Xyes" ]; then
    echo "# [$MyName] (`date`) $job_tag " 2>&1 | tee -a $JOB_LOG
    X=$(qsub $job 2>&1) 
    if [ "X$X" == "X" ]; then
      echo "[$MyName] Error submitting job $job"
      exit 3
    fi 2>&1 | tee -a $JOB_LOG
    echo $X | tee -a $JOB_LOG
    ### qsub $job 2>&1 | tee -a $JOB_LOG
    jobid=$(echo $X | awk -F. '{print $1}')

    sleep 3s
  fi  # end of if with submit

fi  # of if stoch job

###########################################################
# inversion+contraction job
###########################################################
if [ "X$invcon_production" == "Xyes" ]; then

  job_tag=${date_tag}.1
  job=${run_prefix}.${job_tag}.sh

  stoch_io="-r -R"
  mymode=${invcon_mode}

cat << EOF > $job 
#!/bin/bash
#PBS -N ${ensemble_name}_prod_$job_tag
#PBS -l walltime=$walltime_prod
#PBS -l nodes=$nodes_total:ppn=$ppn
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe

export OMP_NUM_THREADS=1

execdir=$execdir
mode_of_op=${mymode}
nproc_per_call=$nproc_per_call

ppn=$ppn
EOF
# nodes_per_call=$nodes_per_call

  #  loop on gauge configurations
  for g in ${Conf[*]}; do

    gfmt=$(printf "%.4d" $g)

    WDIR=$work_path/$g
    mkdir -p $WDIR 

    source_coords_list=($(awk '
        $1=='$g' && $2<'$TT'/'$n_coherent_source' {
          printf("%d,%d,%d,%d\n", $2,$3,$4,$5)
        }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))
    source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
    echo "# [$MyName] source coords number = $source_coords_number"

       cvc_input=${run_prefix}.${job_tag}.input
    tmLQCD_input=invert.input
             out=${run_prefix}.${job_tag}.out
             err=${run_prefix}.${job_tag}.err
   
cat << EOF >> $job

workdir=$WDIR
input=$cvc_input
out=$out
err=$err
stoch_io="$stoch_io"

cd \$workdir
aprun -n \$nproc_per_call -N \$ppn \$execdir/piN2piN_factorized -c \$stoch_io -m \$mode_of_op -f \$input 1>\${out}.\${PBS_JOBID} 2>\${err}.\${PBS_JOBID} &

EOF

    # cvc input file
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/           { print "Nconf = '$g'"; next}
      /^seed[\ =]/            { print "seed = "('${seed_offset}'+'$g'); next}
      /^filename_prefix[\ =]/  { print "filename_prefix = '"$stoch_path"'/'$g'/source"; next}
      /^filename_prefix2[\ =]/ { print "filename_prefix2 = '"$stoch_path"'/'$g'/prop"; next}
      {print}' > $WDIR/$cvc_input

    # append source coords list
    for (( isrc=0; isrc<$source_coords_number; isrc++)); do
      printf "source_coords = %s\n" ${source_coords_list[$isrc]}
    done >> $WDIR/$cvc_input

    cat $tmLQCD_input_mask | awk '
      /^InitialStoreCounter[\ =]/ {print "InitialStoreCounter = '$g'"; next}
      {print}' > $WDIR/$tmLQCD_input

  done  # end of loop on configurations
   
cat << EOF >> $job

echo "# (\$(date))"

wait

exit 0
EOF

  if [ "X$with_submit" == "Xyes" ]; then
    echo "# [$MyName] (`date`) $job_tag " 2>&1 | tee -a $JOB_LOG
    if [ "X$jobid" != "X" ]; then
      echo "# [$MyName] depend on jobid = $jobid"
      qsub -W depend=afterok:$jobid $job 2>&1 | tee -a $JOB_LOG
    else
      qsub $job 2>&1 | tee -a $JOB_LOG
    fi

    sleep 3s
  fi  # end of if with submit

fi  # of if invcon job

done  # end of while on conf charges

echo "# [$MyName] (`date`)"
exit 0
