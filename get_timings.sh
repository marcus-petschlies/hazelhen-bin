#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

log=$MyName.log

echo "# [$MyName] `date`" > $log

file=$1

awk '
  BEGIN{li=0; lt=0}
  /^# [1-9]+ x [1-9]+ x [1-9]+ x [1-9]+/ {nproc=$2*$4*$6*$8; next}
  /# Inversion done in/ && /iterations/ {li++;iter[li]=$5}
  /# Inversion done in/ && /sec\./ {lt++;tinv[lt]=$5}
  END{
    m=0; m2=0;
    for( i=1; i<=li; i++) {
      m  += iter[i]
      m2 += iter[i] * iter[i]
    }
    printf ("iter %6d %6d  %16.7e %16.7e\n", nproc, li, m/li, sqrt( ( m2/li - (m/li)^2)/(li-1) ) )
    #
    m=0; m2=0;
    for( i=1; i<=lt; i++) {
      m  += tinv[i]
      m2 += tinv[i] * tinv[i]
    }
    tb=m/lt
    dt=sqrt( ( m2/lt - (m/lt)^2)/(lt-1) )
    printf ("time %6d %6d  %16.7e %16.7e   %16.7e %16.7e\n", nproc, lt, tb, dt, tb*nproc/3600, dt*nproc/3600 )

  }' $file >> $log

echo "# [$MyName] `date`" >> $log

exit 0

