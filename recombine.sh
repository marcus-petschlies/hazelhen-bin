#!/bin/bash
#PBS -N 
#PBS -l nodes=1:ppn=24
#PBS -l walltime=
#PBS -M marcus.petschlies@hiskp.uni-bonn.de
#PBS -m abe

execdir=/zhome/academic/HLRS/hsk/xskmapet/bin
workdir=
input=
out=
err=

export OMP_NUM_THREADS=24
cd $workdir

aprun $execdir/piN2piN_diagrams_complete -f $input 1>${out}.${PBS_JOBID} 2>${err}.${PBS_JOBID}

es=$?
if [ $es -ne 0 ]; then
  echo "Error from aprun, status was $es"
  exit 1
fi

exit 0
~
