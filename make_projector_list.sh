#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
log=$MyName.log
lst=$MyName.lst

sigma_g0d=( 1 -1 -1 -1  1 -1  1 -1 -1 -1  1  1  1 -1 -1 -1 )

########################################
# get irrep dimension from first
#   character of irrep name
########################################
function get_irrep_dim {
  if   [[ $1 == H* ]]; then echo 4;
  elif [[ $1 == G* ]]; then echo 2;
  elif [[ $1 == K* ]]; then echo 1;
  elif [[ $1 == A* ]]; then echo 1;
  elif [[ $1 == B* ]]; then echo 1;
  elif [[ $1 == E* ]]; then echo 2;
  elif [[ $1 == T* ]]; then echo 3;
  else
    echo "[get_irrep_dim] Error, unknown irrep initial"
    exit 2
  fi
}

tag=""
source_filename=""

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-t") tag=$2; shift 2;;
      "-s") source_filename=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi


echo "# [$MyName] (`date`)" | tee $log

if [ "X$tag" == "X" ] ; then
  echo "[$MyName] Error, need tag"  | tee -a $log
  exit 1
fi

if [ "X$source_filename" == "X" ] ; then
  source_filename="$MyName.in"
  echo "[$MyName] Warning, no source_filename given, using default $source_filename" | tee -a $log
fi

########################################
########################################
##
## set default values
##
########################################
########################################

########################################
# number of sink timeslices
#  ( not including source time )
########################################
src_snk_time_separation=24

########################################
########################################
##
## set default values
## for group projection
##
########################################
########################################

########################################
# list of little groups
########################################
littlegroup_list=( 2Oh 2C4v 2C2v 2C3v )

########################################
# total momentum list per little group
########################################
ptot_list[0]="0,0,0"
ptot_list[1]="0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;"
ptot_list[2]="1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1"
ptot_list[3]="1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"

fbwd_list=( fwd bwd )
d_spin=4
reorder=0

momentum_square_cut=3


cat << EOF | tee -a $log
# [$MyName] tag                     = $tag
# [$MyName] source_filename         = $source_filename
# [$MyName] src_snk_time_separation = $src_snk_time_separation
# [$MyName]
# [$MyName] d_spin                  = $d_spin
# [$MyName] reorder                 = $reorder
# [$MyName] fbwd_list               = ${fbwd_list[*]}
# [$MyName] 
EOF

if [ -e $source_filename ]; then
  echo "# [$MyName] source from file $source_filename"  | tee -a $log
  source $source_filename
else
  echo "[$MyName] Warning, nothing to source"  | tee -a $log
fi

########################################
# check length of lists for consistency
########################################

num_mf=$( echo ${ptot_list[*]}        | wc -w )
num_lg=$( echo ${littlegroup_list[*]} | wc -w )
num_ir=$( echo ${irrep_list[*]}       | wc -w )
if [ $num_mf -ne $num_lg ] || [ $num_mf -ne $num_ir ] || [ $num_lg -ne $num_ir ];
then
  echo "[$MyName] Error, numbers of frames $num_mf, little groups $num_lg and irreps $num_ir do not match" | tee -a $log
  exit 2
else
  echo "# [$MyName] number of frames $num_mf == number of little groups $num_lg" | tee -a $log
fi
 
########################################
########################################
##
## start the list file
##
########################################
########################################
echo "# [$MyName] (`date`)" > $lst

########################################
# initialize new projector list file
########################################
echo "# [$MyName] (`date`)" > $MyName.lst


########################################
if [ "X$tag" == "XN-N" ]; then
########################################

  type="b-b"

  gi1_list=(  14,4  11,5  8,4 )
  gf1_list=(  14,4  11,5  8,4 )

  diagrams="n1,n2"

  if [ -e $source_filename ]; then
    source $source_filename
  else
    echo "[$MyName] Warning, could not find file $source_filename"
  fi | tee -a $log

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

for((ilg=0; ilg<$num_lg; ilg++)); do
  lg=${littlegroup_list[$ilg]}

  if [ "X$lg" == "X" ]; then
    echo "[$MyName] Error, lg is empty"
    exit 4
  fi | tee -a $log

  nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
  echo "# [$MyName] lg = $lg nptot = $nptot" | tee -a  $log

  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

    ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )
    if [ "X$ptot" == "X" ]; then
      echo "[$MyName] Error, ptot is empty"  | tee -a $log
      exit 5
    fi

    pf1=$ptot
    pi1=$(echo $ptot | tr ',' ' ' | awk '{printf("%d,%d,%d", -$1, -$2, -$3)}')
 
    echo "# [$MyName] lg = $lg ptot = $ptot pi1 = $pi1 pf1 = $pf1" | tee -a $log

    nirrep=$(echo ${irrep_list[$ilg]} | tr ';' ' ' | wc -w)

    for(( iirrep=0; iirrep<$nirrep; iirrep++));
    do

      irrep=$( echo ${irrep_list[$ilg]} | awk -F\; '{print $('$iirrep'+1)}' )
      if [ "X$irrep" == "X" ]; then
        echo "[$MyName] Error, irrep is empty" | tee -a $log
        exit 6
      fi

      irrep_dim=$( get_irrep_dim $irrep )

      echo "# [$MyName] lg = $lg ptot = $ptot irrep = $irrep irrep_dim = $irrep_dim" | tee -a $log

      for fbwd in ${fbwd_list[*]}; do

        for gi1 in ${gi1_list[*]}; do
        for gf1 in ${gf1_list[*]}; do

cat << EOF >> $lst

BeginTwopointFunctionInit
  irrep    = $irrep
  pi1      = $pi1
  pf1      = $pf1
  gi1      = $gi1
  gf1      = $gf1
  group    = $lg
  fbwd     = $fbwd
EndTwopointFunction

EOF
        done  # of gf1
        done  # of gi1
      done  # of fbwd
    done  # of irrep
  done  # of ptot
done  # of ilg

########################################
########################################


########################################
elif [ "X$tag" == "XD-D" ]; then
########################################

  type="b-b"

  gi1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="d1,d2,d3,d4,d5,d6"

  ########################################
  # source input file
  ########################################
  if [ -e $source_filename ]; then
    source $source_filename
  else
    echo "[$MyName] Warning, could not find file $source_filename" | tee -a $log 
  fi

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" | tee -a $log

cat << EOF >> $lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

for((ilg=0; ilg<$num_lg; ilg++)); do
  lg=${littlegroup_list[$ilg]}

  if [ "X$lg" == "X" ]; then
    echo "[$MyName] Error, lg is empty"  | tee -a $log
    exit 4
  fi

  nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
  echo "# [$MyName] lg = $lg nptot = $nptot" | tee -a $log

  for(( iptot=0; iptot<$nptot; iptot++ ));
  do

    ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )
    if [ "X$ptot" == "X" ]; then
      echo "[$MyName] Error, ptot is empty"  | tee -a $log
      exit 5
    fi

    pf1=$ptot
    pi1=$(echo $ptot | tr ',' ' ' | awk '{printf("%d,%d,%d", -$1, -$2, -$3)}')
 
    echo "# [$MyName] lg = $lg ptot = $ptot pi1 = $pi1 pf1 = $pf1" | tee -a $log

    nirrep=$(echo ${irrep_list[$ilg]} | tr ';' ' ' | wc -w)

    for(( iirrep=0; iirrep<$nirrep; iirrep++));
    do

      irrep=$( echo ${irrep_list[$ilg]} | awk -F\; '{print $('$iirrep'+1)}' )
      if [ "X$irrep" == "X" ]; then
        echo "[$MyName] Error, irrep is empty" | tee -a $log
        exit 6
      fi

      irrep_dim=$( get_irrep_dim $irrep )

      echo "# [$MyName] lg = $lg ptot = $ptot irrep = $irrep irrep_dim = $irrep_dim" | tee -a $log

      for fbwd in ${fbwd_list[*]}; do

        for gi1 in ${gi1_list[*]}; do
          
          gi1_12=($( echo $gi1 | tr ',' ' ' ))

          norm_str="  # norm     ="
          s0d=${sigma_g0d[${gi1_12[0]}]}
          if [ $s0d -ne 1 ]; then
            norm_str="  norm     = $s0d"
            for((i=1; i<$num_diag; i++));do norm_str="${norm_str},${sigma_g0d[${gi1_12[0]}]}"; done
          fi

        for gf1 in ${gf1_list[*]}; do

          gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF >> $lst

BeginTwopointFunctionInit
  irrep    = $irrep
  pi1      = $pi1
  pf1      = $pf1
  gi1      = $gi1
  gf1      = $gf1
  group    = $lg
  fbwd     = $fbwd
$norm_str
EndTwopointFunction

EOF
        done  # of gf1
        done  # of gi1
      done  # of fbwd
    done  # of irrep
  done  # of ptot
done  # of ilg

########################################
########################################

########################################
elif [ "X$tag" == "XpixN-D" ]; then
########################################

  type="mxb-b"

  pi2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"


  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=(  9,4  0,4  7,4 13,4  4,4 15,4 )

  diagrams="t1,t2,t3,t4,t5,t6"

  source $source_filename

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" >> $log

cat << EOF | tee -a $MyName.lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

  for((ilg=0; ilg<$num_lg; ilg++)); do
    lg=${littlegroup_list[$ilg]}

    if [ "X$lg" == "X" ]; then
      echo "[$MyName] Error, lg is empty"  | tee -a $log
      exit 4
    fi

    nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
    echo "# [$MyName] lg = $lg nptot = $nptot" >> $log

    # loop on ptot = total momentum = pf1
    for(( iptot=0; iptot<$nptot; iptot++ ));
    do

      ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )

      ptot_vec=($( echo $ptot | tr ',' ' ' ))
      pf1=$ptot

      # loop on pi2
      num_pi2=$( echo ${pi2_list} | tr ';' ' ' | wc -w )
      for(( ipi2=0; ipi2<$num_pi2; ipi2++ )); 
      do
        pi2=$( echo ${pi2_list} | awk -F\; '{print $('$ipi2'+1)}' )

        pi2_vec=($( echo $pi2 | tr ',' ' ' ))
        pi1_vec=( $(( -${ptot_vec[0]} - ${pi2_vec[0]} )) $(( -${ptot_vec[1]} - ${pi2_vec[1]} )) $(( -${ptot_vec[2]} - ${pi2_vec[2]} )) )

        pi1=$( echo ${pi1_vec[*]} | awk '{printf("%d,%d,%d", $1, $2, $3)}')
 
        pi1_norm2=$(( ${pi1_vec[0]} * ${pi1_vec[0]} +  ${pi1_vec[1]} * ${pi1_vec[1]} +  ${pi1_vec[2]} * ${pi1_vec[2]} ))
        if [ $pi1_norm2 -gt $momentum_square_cut ]; then
          echo "# [$MyName] discard pi1 = $pi1"
          continue
        fi

        echo "# [$MyName] lg = $lg ptot = $ptot pi1 = $pi1  pi2 = $pi2  pf1 = $pf1" >> $log

        nirrep=$(echo ${irrep_list[$ilg]} | tr ';' ' ' | wc -w)
        echo "# [$MyName] nirrep = $nirrep"

        # loop on irreps
        for(( iirrep=0; iirrep<$nirrep; iirrep++)) ; do

          irrep=$( echo ${irrep_list[$ilg]} | awk -F\; '{print $('$iirrep'+1)}' )
          if [ "X$irrep" == "X" ]; then
            echo "[$MyName] Error, irrep is empty" | tee -a $log
            exit 6
          fi

          irrep_dim=$( get_irrep_dim $irrep )

          echo "# [$MyName] lg = $lg ptot = $ptot irrep = $irrep irrep_dim = $irrep_dim" >> $log

          # loop on forward, backward
          for fbwd in ${fbwd_list[*]}; do

 
            # loop on gamma structures
            for gi1 in ${gi1_list[*]}; do
          
              gi1_12=($( echo $gi1 | tr ',' ' ' ))

            for gi2 in ${gi2_list[*]}; do

            for gf1 in ${gf1_list[*]}; do

              gf1_12=($( echo $gf1 | tr ',' ' ' ))

cat << EOF | tee -a $MyName.lst

BeginTwopointFunctionInit
  irrep    = $irrep
  pi1      = $pi1
  pi2      = $pi2
  pf1      = $pf1
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  group    = $lg
  fbwd     = $fbwd
EndTwopointFunction

EOF
            done  # of gf1
            done  # of gi2
            done  # of gi1
          done  # of fbwd
        done  # of irrep
      done  # of pi1 
    done  # of ptot
  done  # of ilg

########################################
########################################

########################################
elif [ "X$tag" == "XpixN-pixN" ]; then
########################################

  type="mxb-mxb"

  pi2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"
  pf2_list="0,0,0;0,0,1;0,0,-1;1,0,0;-1,0,0;0,1,0;0,-1,0;1,1,0;-1,-1,0;1,0,1;-1,0,-1;0,1,1;0,-1,-1;-1,1,0;1,-1,0;1,0,-1;-1,0,1;0,-1,1;0,1,-1;1,1,1;-1,-1,-1;1,1,-1;-1,-1,1;1,-1,1;-1,1,-1;-1,1,1;1,-1,-1"


  gi1_list=( 14,4  11,5  8,4 )
  gi2_list=(  5 )
  gf1_list=( 14,4  11,5  8,4 )
  gf2_list=(  5 )

  diagrams="b1,b2,w1,w2,w3,w4,z1,z2,z3,z4,s1,s2"

  source $source_filename

  num_diag=$(echo $diagrams | tr ',' ' ' | wc -w )
  echo "# [$MyName] diagrams = $diagrams, num_diag = $num_diag" >> $log

cat << EOF | tee -a $MyName.lst

BeginTwopointFunctionGeneric
  n        = $num_diag
  d        = $d_spin
  type     = $type
  tag      = $tag
  reorder  = $reorder
  T        = $src_snk_time_separation
  diagrams = $diagrams
EndTwopointFunction

EOF

  for((ilg=0; ilg<$num_lg; ilg++)); do
    lg=${littlegroup_list[$ilg]}

    if [ "X$lg" == "X" ]; then
      echo "[$MyName] Error, lg is empty"  | tee -a $log
      exit 4
    fi

    nptot=$( echo ${ptot_list[$ilg]} | tr ';' ' ' | wc -w )
    echo "# [$MyName] lg = $lg nptot = $nptot" >> $log

    # loop on ptot = total momentum = pf1
    for(( iptot=0; iptot<$nptot; iptot++ ));
    do

      ptot=$( echo ${ptot_list[$ilg]} | awk -F\; '{print $('$iptot'+1)}' )

      ptot_vec=($( echo $ptot | tr ',' ' ' ))

      # loop on pf2
      num_pf2=$( echo ${pf2_list} | tr ';' ' ' | wc -w )
      for(( ipf2=0; ipf2<$num_pf2; ipf2++ ));
      do
        pf2=$( echo ${pf2_list} | awk -F\; '{print $('$ipf2'+1)}' )

        pf2_vec=($( echo $pf2 | tr ',' ' ' ))
        pf1_vec=( $(( ${ptot_vec[0]} - ${pf2_vec[0]} )) $(( ${ptot_vec[1]} - ${pf2_vec[1]} )) $(( ${ptot_vec[2]} - ${pf2_vec[2]} )) )

        pf1=$( echo ${pf1_vec[*]} | awk '{printf("%d,%d,%d", $1, $2, $3)}')

        pf1_norm2=$(( ${pf1_vec[0]} * ${pf1_vec[0]} +  ${pf1_vec[1]} * ${pf1_vec[1]} +  ${pf1_vec[2]} * ${pf1_vec[2]} ))
        if [ $pf1_norm2 -gt $momentum_square_cut ]; then
          echo "# [$MyName] discard pf1 = $pf1"
          continue
        fi

      # loop on pi2
      num_pi2=$( echo ${pi2_list} | tr ';' ' ' | wc -w )
      for(( ipi2=0; ipi2<$num_pi2; ipi2++ )); 
      do
        pi2=$( echo ${pi2_list} | awk -F\; '{print $('$ipi2'+1)}' )

        pi2_vec=($( echo $pi2 | tr ',' ' ' ))
        pi1_vec=( $(( -${ptot_vec[0]} - ${pi2_vec[0]} )) $(( -${ptot_vec[1]} - ${pi2_vec[1]} )) $(( -${ptot_vec[2]} - ${pi2_vec[2]} )) )

        pi1=$( echo ${pi1_vec[*]} | awk '{printf("%d,%d,%d", $1, $2, $3)}')
 
        pi1_norm2=$(( ${pi1_vec[0]} * ${pi1_vec[0]} +  ${pi1_vec[1]} * ${pi1_vec[1]} +  ${pi1_vec[2]} * ${pi1_vec[2]} ))
        if [ $pi1_norm2 -gt $momentum_square_cut ]; then
          echo "# [$MyName] discard pi1 = $pi1"
          continue
        fi

        echo "# [$MyName] lg = $lg ptot = $ptot   pi1 = $pi1   pi2 = $pi2   pf1 = $pf1   pf2 = $pf2" >> $log

        nirrep=$(echo ${irrep_list[$ilg]} | tr ';' ' ' | wc -w)
        echo "# [$MyName] nirrep = $nirrep"

        # loop on irreps
        for(( iirrep=0; iirrep<$nirrep; iirrep++)) ; do

          irrep=$( echo ${irrep_list[$ilg]} | awk -F\; '{print $('$iirrep'+1)}' )
          if [ "X$irrep" == "X" ]; then
            echo "[$MyName] Error, irrep is empty" | tee -a $log
            exit 6
          fi

          irrep_dim=$( get_irrep_dim $irrep )

          echo "# [$MyName] lg = $lg ptot = $ptot irrep = $irrep irrep_dim = $irrep_dim" >> $log

          # loop on forward, backward
          for fbwd in ${fbwd_list[*]}; do

 
            # loop on gamma structures
            for gi1 in ${gi1_list[*]}; do
          
              gi1_12=($( echo $gi1 | tr ',' ' ' ))

            for gi2 in ${gi2_list[*]}; do

            for gf1 in ${gf1_list[*]}; do

              gf1_12=($( echo $gf1 | tr ',' ' ' ))

            for gf2 in ${gf2_list[*]}; do

cat << EOF >> $MyName.lst

BeginTwopointFunctionInit
  irrep    = $irrep
  pi1      = $pi1
  pi2      = $pi2
  pf1      = $pf1
  pf2      = $pf2
  gi1      = $gi1
  gi2      = $gi2
  gf1      = $gf1
  gf2      = $gf2
  group    = $lg
  fbwd     = $fbwd
EndTwopointFunction

EOF
            done  # of gf2
            done  # of gf1
            done  # of gi2
            done  # of gi1
          done  # of fbwd
        done  # of irrep
      done  # of pi2 
      done  # of pf2
    done  # of ptot
  done  # of ilg

else
  echo "[$MyName] Error, unrecognized tag" | tee -a $log
  exit 11
fi

echo "# [$MyName] (`date`)" >> $lst

echo "# [$MyName] (`date`)" | tee -a $log
exit 0
