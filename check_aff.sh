#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log
echo "# [$MyName] (`date`)" | tee $log

Conf=()

path_to_data=.

# usual values for cA2.09.48
#
#       piN_piN_size=47743079893 
#   piN_piN_oet_size=13766882803 
#     piN_piN_number=4
# piN_piN_oet_number=4

piN_piN_size=
piN_piN_oet_size=
piN_piN_number=
piN_piN_oet_number=

source $MyName.in

if [ "X$piN_piN_number" == "X" ] || [ "X$piN_piN_oet_number" == "X" ]; then
  echo "[$MyName] Error, need piN_piN_number and piN_piN_oet_number"
  exit 1
fi

if [ "X$piN_piN_size" == "X" ] || [ "X$piN_piN_oet_size" == "X" ]; then
  echo "[$MyName] Error, need piN_piN_size and piN_piN_oet_size"
  exit 1
fi

cat << EOF | tee -a $log
# [$MyName] path_to_data       = $path_to_data
# [$MyName]
# [$MyName] piN_piN_size       = $piN_piN_size
# [$MyName] piN_piN_oet_size   = $piN_piN_oet_size
# [$MyName]
# [$MyName] piN_piN_number     = $piN_piN_number
# [$MyName] piN_piN_oet_number = $piN_piN_oet_number
# [$MyName]
# [$MyName] Conf               = ${Conf[*]}

EOF
sleep 1s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi


for i in 0; do
  printf "# conf --- number of files --- number of files is okay --- filesize is okay --- number of oet files --- number of oet files is okay --- oet filesize is okay\n"
  printf "# =======================================================================================================================================\n\n" >> $log
done >> $log

echo "# [$MyName] `date`" > $MyName.filelist

for g in ${Conf[*]}; do
  gfmt=`printf "%.4d" $g`

  file_list=($(ls -rt $path_to_data/$g/piN_piN.${gfmt}.tsrc??.aff 2>/dev/null | tr ' ' '\n' | head -n $piN_piN_number ))
  nf=$(echo ${file_list[*]} | wc -w)
  if [ $nf -ne $piN_piN_number ]; then
    file_number_ok=0
  else
    file_number_ok=1
  fi
  for(( i=0; i<$piN_piN_number; i++)); do
    file_size_ok[$i]=0
  done
  for((i=0; i<$nf; i++)); do
    f=${file_list[$i]}
    s=$(stat -c %s $f)
    if [ $s -lt  $piN_piN_size ]; then
      file_size_ok[$i]=0
    else
      file_size_ok[$i]=1
      echo $f >> $MyName.filelist
    fi
  done

  file_list_oet=($(ls -rt $path_to_data/$g/piN_piN_oet.${gfmt}.tsrc??.aff 2>/dev/null | tr ' ' '\n' | head -n $piN_piN_oet_number ))
  nf_oet=$(echo ${file_list_oet[*]} | wc -w)
  if [ $nf_oet -ne $piN_piN_oet_number ]; then
    file_number_oet_ok=0
  else
    file_number_oet_ok=1
  fi

  for(( i=0; i<$piN_piN_oet_number; i++)); do
    file_size_oet_ok[$i]=0
  done
  for((i=0; i<$nf_oet; i++)); do
    f=${file_list_oet[$i]}
    s=$(stat -c %s $f)
    if [ $s -lt  $piN_piN_oet_size ]; then
      file_size_oet_ok[$i]=0
    else
      file_size_oet_ok[$i]=1
      echo $f >> $MyName.filelist
    fi
  done
  printf "%6d | %3d %3d | " $g $nf $file_number_ok
  for(( i=0; i<$piN_piN_number; i++)); do
    printf "%3d " ${file_size_ok[$i]}
  done
  printf "| %3d %3d | " $nf_oet $file_number_oet_ok
  for(( i=0; i<$piN_piN_oet_number; i++)); do
    printf "%3d " ${file_size_oet_ok[$i]}
  done
  printf "\n"
  all_okay=
  if [ "X$(echo $file_number_ok ${file_size_ok[*]} | grep 0)" == "X" ]; then
    all_okay=1
  else
    all_okay=0
  fi

  all_oet_okay=
  if [ "X$(echo $file_number_oet_ok ${file_size_oet_ok[*]} | grep 0)" == "X" ]; then
    all_oet_okay=1
  else
    all_oet_okay=0
  fi

  printf "%6d all okay %3d %3d\n\n" $g $all_okay $all_oet_okay
  printf "# ---------------------------------------------------------------------------------------------------------------------------------------\n"
done >> $log

  printf "\n# =======================================================================================================================================\n\n" >> $log

echo "# [$MyName] (`date`) all done" | tee -a $log

exit 0
