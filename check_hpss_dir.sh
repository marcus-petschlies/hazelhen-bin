#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log


piN_piN_size=63654144469

piN_piN_oet_size=18355427827

source_size=6442451704
prop_size=6442451704

Conf=( 268 528 624 632 )

TT=128
Th=$(( $TT / 2 ))
src_pos_file=source_locations.cA2.0900.64.b.tbase8.ncoherent2.tab

if ! [ -e $src_pos_file ]; then
  echo "[$MyName] Error, need src pos file"
  exit 1
fi

for d in ${Conf[*]}; do

  dfmt=`printf "%.4d" $d`

  hpss_file=hpss.$d

  # src_pos_str=$(awk '$1=='$d' && $2<'$Th' {printf("t%.2dx%.2dy%.2dz%.2d  ", $2, $3, $4, $5)}' $src_pos_file )
  tsrc_str_lst=$(awk '$1=='$d' && $2<'$Th' {printf("%.2d  ", $2)}' $src_pos_file )

  # echo "# [$MyName] $dfmt ---> ${tsrc_pos_str[*]}"

  f1size=""
  if [ "X${tsrc_str_lst[*]}" == "X" ]; then
   f1size="NA"
   f2size="NA"
  fi
  for t in ${tsrc_str_lst[*]} ;do

    f1=piN_piN.${dfmt}.tsrc${t}.aff

    s="$(awk '$NF=="'$f1'" {print $5}' $hpss_file )"

    if  [ "X$s" == "X" ]; then
      f1size="${f1size}0"
    elif [ $s -lt $piN_piN_size ]; then
      f1size="${f1size}0"
    else
      f1size="${f1size}1"
    fi
  done

  f2size=""
  for t in ${tsrc_str_lst[*]} ;do

    f2=piN_piN_oet.${dfmt}.tsrc${t}.aff
    s="$(awk '$NF=="'$f2'" {print $5}' $hpss_file )"

    if  [ "X$s" == "X" ]; then
      f2size="${f2size}0"
    elif [ $s -lt $piN_piN_oet_size ]; then
      f2size="${f2size}0"
    else
      f2size="${f2size}1"
    fi

  done

  ssize=""
  for((i=0; i<12;i++)); do
    f=source.$dfmt.`printf "%.5d" $i`
    s="$(awk '$NF=="'$f'" {print $5}' $hpss_file )"
    if  [ "X$s" == "X" ]; then
      ssize="${ssize}0"
    elif [ $s -lt $source_size ]; then
      ssize="${ssize}0"
    else
      ssize="${ssize}1"
    fi
  done

  psize=""
  for((i=0; i<12;i++)); do
    f=prop.$dfmt.`printf "%.5d" $i`
    s="$(awk '$NF=="'$f'" {print $5}' $hpss_file )"
    if  [ "X$s" == "X" ]; then
      psize="${psize}0"
    elif [ $s -lt $prop_size ]; then
      psize="${psize}0"
    else
      psize="${psize}1"
    fi
  done

  printf "%6d %10s %10s %16s %16s\n" $d $f1size $f2size $ssize $psize

done 2>&1 | tee -a $log

exit 0
