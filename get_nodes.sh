#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

f=$1
echo "# [$MyName] $f" > ${MyName}.msg
awk '/Process/ && /nid/' $f >> ${MyName}.msg

echo "# [$MyName] $f" > ${MyName}.lst
awk '/nid/ {sub(/nid[0]+/,"",$7); sub(/:/,"",$7); print $7}' ${MyName}.msg | sort -un >> ${MyName}.lst

exit 0
