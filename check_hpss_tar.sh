#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')
log=${MyName}.`date +%Y_%m_%d_%H_%M`.log


# aff_size=656076605440
aff_size=656076595200

# srcprop_size=154837954560
# srcprop_size=154815252480
# srcprop_size=154815088640
srcprop_size=154792458240

prefix=piN2piNcA20964b

Conf=( 168 172 176 180 184 188 192 196 200 204 208 212 216 220 224 228 232 236 240 244 248 252 256 260 264 268 272 276 280 284 288 292 296 300 304 308 312 316 320 324 328 332 336 340 344 348 352 356 360 364 368 372 376 380 384 388 392 396 400 404 408 412 416 420 424 428 432 436 440 444 448 452 456 460 464 468 472 476 480 484 488 492 496 500 504 508 512 516 520 524 528 532 536 540 544 548 552 556 560 564 568 572 576 580 584 588 592 596 600 604 608 612 616 620 624 628 632 636 640 644 648 652 656 660 664 668 672 676 680 684 688 692 696 700 )


TT=128
Th=$(( $TT / 2 ))
src_pos_file=source_locations.cA2.0900.64.b.tab

hpss_file=/zhome/academic/HLRS/hsk/xskmapet/xskfpitt.hpss.cA2.09.64.b

for d in ${Conf[*]}; do

  dfmt=`printf "%.4d" $d`

  # src_pos_str=$(awk '$1=='$d' && $2<'$Th' {printf("t%.2dx%.2dy%.2dz%.2d  ", $2, $3, $4, $5)}' $src_pos_file )
  # tsrc_str_lst=$(awk '$1=='$d' && $2<'$Th' {printf("%.2d  ", $2)}' $src_pos_file )

  # echo "# [$MyName] $dfmt ---> ${tsrc_pos_str[*]}"

  f=${prefix}_${d}_aff.tar.gz
  s=$(awk '$NF=="'$f'" {print $5}' $hpss_file )
  if  [ "X$s" == "X" ]; then
    aff_size_ok=0
  elif [ $s -lt $aff_size ]; then
    aff_size_ok=0
  else
    aff_size_ok=1
  fi
  
  f=${prefix}_${d}_propsource_output.tar.gz
  s=$(awk '$NF=="'$f'" {print $5}' $hpss_file)
  if  [ "X$s" == "X" ]; then
    srcprop_size_ok=0
  elif [ $s -lt $srcprop_size ]; then
    srcprop_size_ok=0
  else
    srcprop_size_ok=1
  fi


  printf "%6d %2s %2s\n" $d $aff_size_ok $srcprop_size_ok

done 2>&1 | tee -a $log

exit 0
