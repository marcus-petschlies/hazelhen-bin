#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh/,"",$NF);print $NF}')

echo "# [$MyName] (`date`)"

Conf=( )

work_path=$PWD

run_prefix="projection"

job_mask=$work_path/${run_prefix}.sh
cvc_input_mask=$work_path/${run_prefix}.input

source_coords_filename=
source_coords_select=( 1 0 )

num_threads=24

n_coherent_source=2
with_submit=no
ensemble_name=
src_per_job=
walltime=24:00:00
data_file_path=
twopoint_function_tag=

lg_file=$work_path/little_groups_2Oh.tab

LL=
TT=
JOB_LOG=$work_path/JOB_LOG
echo "# [$MyName] JOB_LOG = $JOB_LOG"

if [ $# -eq 0 ]; then
  echo -e "# [$MyName] WARNING: no command line arguments provided, keeping default values\n"
else
  while [ "$1" ]; do
    case "$1" in
      "-L") LL=$2; shift 2;;
      "-T") TT=$2; shift 2;;
      "-i") cvc_input_mask=$2; shift 2;;
      "-j") job_mask=$2; shift 2;;
      "-s") source_coords_filename=$2; shift 2;;
      "-m") src_per_job=$2; shift 2;;
      "-n") n_coherent_source=$2; shift 2;;
      "-x") with_submit=$2; shift 2;;
      "-e") ensemble_name=$2; shift 2;;
      "-w") walltime=$2; shift 2;;
      "-d") work_path=$2; shift 2;;
      "-p") data_file_path=$2; shift 2;;
      *) exit 1;;
    esac
  done
fi

source $MyName.in

      job_mask=$work_path/${run_prefix}.sh
cvc_input_mask=$work_path/${run_prefix}.input

if [ "X$src_per_job" == "X" ]; then
  echo "[$MyName] need number of sources per job"
  exit 1
fi

if [ "X$LL" == "X" ] || [ "X$TT" == "X" ] ; then
  echo "# [$MyName] Error, need L and T"
  exit 1
fi

if [ "X$ensemble_name" == "X" ] ; then
  echo "# [$MyName] Error, need ensemble name"
  exit 1
fi

if [ "X$source_coords_filename" == "X" ] ; then
  echo "# [$MyName] Error, need source coords filename"
  exit 1
fi

if [ "X$data_file_path" == "X" ] ; then
  echo "# [$MyName] Error, need data file path"
  exit 1
fi

cat << EOF

###########################################################
###########################################################
##                                                       ##
## [$MyName] did you check the input files?   ##
##                                                       ##
###########################################################
###########################################################
#
# [$MyName] with_submit           = $with_submit
# [$MyName] ensemble name         = $ensemble_name
# [$MyName] work_path             = $work_path
# [$MyName] data_file_path        = $data_file_path
# [$MyName]
# [$MyName] L                     = $LL
# [$MyName] T                     = $TT
# [$MyName]
# [$MyName] source coords file    = $source_coords_filename
# [$MyName] source coords select  = ${source_coords_select[0]}, ${source_coords_select[1]}
# [$MyName]
# [$MyName] job mask              = $job_mask
# [$MyName] input mask            = $cvc_input_mask
# [$MyName]
# [$MyName] Conf                  = ${Conf[*]}
# [$MyName]
# [$MyName] n_coherent source     = $n_coherent_source
# [$MyName] src_per_job           = $src_per_job
# [$MyName] walltime              = $walltime
# [$MyName]
# [$MyName] twopoint_function_tag = $twopoint_function_tag 
# [$MyName]
EOF

sleep 2s
echo "# [$MyName] Okay ? (yes)"
read answer
if [ "X$answer" != "Xyes" ]; then
  echo "[$MyName] abort"
  exit 1
else
  echo "# [$MyName] continue"
fi



for g in ${Conf[*]}; do

  gfmt=$(printf "%.4d" $g)

  WDIR=$work_path/$g
  mkdir -p $WDIR && cd $WDIR || exit 1

  source_coords_list=($(awk '
      $1=='$g' && $2<'$TT'/'$n_coherent_source' {
        printf("%d,%d,%d,%d\n", $2,$3,$4,$5)
      }' $source_coords_filename | awk '(NR-1) % '${source_coords_select[0]}' == '${source_coords_select[1]}' '))

  source_coords_number=$(echo ${source_coords_list[*]} | wc -w)
  echo "# [$MyName] conf $g source coords number = $source_coords_number"

  njob=$(( ( $source_coords_number + $src_per_job -1 ) / $src_per_job ))
  echo "# [$MyName] conf $g number of jobs = $njob"

  ln -sf $lg_file .

  for (( i=0; i<$njob; i++)); do

    date_tag=`date +%Y_%m_%d_%H_%M_%S`


    job=${run_prefix}.${date_tag}.sh
    cvc_input=${run_prefix}.${date_tag}.input
    out=${run_prefix}.${date_tag}.out
    err=${run_prefix}.${date_tag}.err

    ###########################################################
    # fill job script file
    ###########################################################
    cat $job_mask | awk '
      /^#PBS -N /         { print "#PBS -N '"$ensemble_name"'_'"${run_prefix}"'_'$gfmt'_'$i'"; next}
      /^#PBS -l walltime/ { print "#PBS -l walltime='"$walltime"'"; next}
      /^workdir=/         { print "workdir='$WDIR'"; next}
      /^input=/           { print "input='$cvc_input'"; next}
      /^out=/             { print "out='$out'"; next}
      /^err=/             { print "err='$err'"; next}
      /^num_threads=/     { print "num_threads='$num_threads'"; next}
      {print}' > $job

    ###########################################################
    # fill cvc input file
    ###########################################################
    cat $cvc_input_mask | awk '
      /^Nconf[\ =]/                  { print "Nconf = '$g'"; next}
      /^seed[\ =]/                   { print "# seed = "; next}
      /^coherent_source_number[\ =]/ { print "coherent_source_number = '"$n_coherent_source"'"; next}
      /^filename_prefix[\ =]/        { print "filename_prefix = '"$data_file_path/$g/piN_piN_diagrams"'"; next}
      /^num_threads[\ =]/            { print "num_threads = '$num_threads'"; next}
      /^T[\ =]/                      { print "T  = '$TT'"; next}
      /^LX[\ =]/                     { print "LX = '$LL'"; next}
      /^LY[\ =]/                     { print "LY = '$LL'"; next}
      /^LZ[\ =]/                     { print "LZ = '$LL'"; next}
      {print}' > $cvc_input

    ###########################################################
    # add source coords list to cvc input file
    ###########################################################
    for (( isrc=$(( $i * $src_per_job )); isrc<$(( ($i + 1) * $src_per_job )); isrc++)); do
      if [ "X${source_coords_list[$isrc]}" != "X" ]; then
        printf "source_coords = %s\n" ${source_coords_list[$isrc]}
      fi
    done  >> $cvc_input

    ###########################################################
    # add twopoint_function list to cvc input file
    ###########################################################
    ${work_path}/make_projector_list.sh $twopoint_function_tag ${work_path}/make_projector_list.in  >> $cvc_input

    ###########################################################
    # submit job if with_submit is yes
    ###########################################################
    if [ "X$with_submit" == "Xyes" ]; then
      echo "# [$MyName] (`date`) $g"  2>&1 | tee -a $JOB_LOG
      qsub $job  2>&1 | tee -a $JOB_LOG

      sleep 2s
    fi

  done  # end of loop on jobs
done  # end of loop on configs

echo "# [$MyName] (`date`)"
exit 0
